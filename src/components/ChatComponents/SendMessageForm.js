import { useState } from 'react';

const SendMessageForm = ({ sendMessage }) => {
    const [message, setMessage] = useState('');

    return <form
        onSubmit={e => {
            e.preventDefault();
            sendMessage(message);
            setMessage('');
        }}>
        <div>
            <input type="user" placeholder="message..."
                onChange={e => setMessage(e.target.value)} value={message} />
            <div>
                <button variant="primary" type="submit" disabled={!message}>Send</button>
            </div>
        </div>
    </form>
}

export default SendMessageForm;
