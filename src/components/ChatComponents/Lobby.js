import { useEffect, useState } from 'react';
//import { playerSessionClearAction } from "../../store/actions/sessionActions";
import { useAppSelector } from "../../hooks";

const Lobby = ({ joinRoom, isSquad }) => {
    const { username } = useAppSelector(state => state.session);
    const { isHuman } = useAppSelector(state => state.playerSessionReducer)
    const { game } = useAppSelector(state => state.game);
    const { squad } = useAppSelector(state => state.getSquadByPlayerReducer);

    const roomType = () => {
        if (isSquad && squad) { 
            if (squad.isHuman && isHuman) {
                return `${squad.id} ${squad.name}`
            } if (!squad.isHuman && !isHuman) {
                return `${squad.id} ${squad.name}`
            }
        } else if (!isHuman && game !== null) {
            return `${game.id} ${game.name} for Zombie players`
        } else if (isHuman && game !== null) {
            return `${game.id} ${game.name} for Human players`
        } else {
            return "the chatroom"
        }
    }

    const room = roomType();

    useEffect(() => {
        joinRoom(username, room);
    }, []);

    return null
}

export default Lobby;
