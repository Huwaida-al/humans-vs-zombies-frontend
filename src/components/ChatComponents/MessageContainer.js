import { useEffect, useRef } from 'react';
import "./Chat.css"

const MessageContainer = ({ messages }) => {
    const messageRef = useRef();

    useEffect(() => {
        if (messageRef && messageRef.current) {
            const { scrollHeight, clientHeight } = messageRef.current;
            messageRef.current.scrollTo({ left: 0, top: scrollHeight - clientHeight, behavior: 'smooth' });
        }
    }, [messages]);

    return( 
    <div ref={messageRef} className='container message-container m-auto p-4' >
        {messages.map((m, index) => (
            <div key={index} className='user-message'>
                <div className='from-user' style={{color: "blue"}}>{m.user}</div>
                <div className='message' style={{color: "black"}}>{m.message}</div>
            </div>
        ))}
    </div>
    )
}

export default MessageContainer;
