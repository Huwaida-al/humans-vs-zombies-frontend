import { useState } from 'react';
import { HubConnectionBuilder, LogLevel } from '@microsoft/signalr';
import {Link} from 'react-router-dom';
import SendMessageForm from './SendMessageForm';
import MessageContainer from './MessageContainer';
import ConnectedUsers from './ConnectedUsers';
import Lobby from './Lobby';
import './Chat.css'
import NavigationBar from '../../hoc/NavigationBar';
import { DeployURL } from '../../http-common';


const ChatComp = ({ sendMessage, messages, users }) => <div>
    {/* <ConnectedUsers users={users} /> */}
    <div className='chat'>
        <MessageContainer messages={messages} />
        <SendMessageForm sendMessage={sendMessage} />
    </div>
</div>

const Chat = ({isSquad}) => {
    const [connection, setConnection] = useState();
    const [messages, setMessages] = useState([]);
    const [users, setUsers] = useState([]);

    const joinRoom = async (user, room) => {
        try {
            const connection = new HubConnectionBuilder()
                .withUrl("https://appservicebackend.azurewebsites.net/hubs/chat")
                .configureLogging(LogLevel.Information)
                .build();

            connection.on("ReceiveMessage", (user, message) => {
                setMessages(messages => [...messages, { user, message }]);
            });

            connection.on("UsersInRoom", (users) => {
                setUsers(users);
            });

            connection.onclose(e => {
                setConnection();
                setMessages([]);
                setUsers([]);
            });

            await connection.start();
            await connection.invoke("JoinRoom", { user, room });
            setConnection(connection);
        } catch (e) {
            console.log(e);
        }
    }

    const sendMessage = async (message) => {
        try {
            await connection.invoke("SendMessage", message);
        } catch (e) {
            console.log(e);
        }
    }

    const closeConnection = async () => {
        try {
            await connection.stop();
        } catch (e) {
            console.log(e);
        }
    }

    return (
    <div className='pb-3'>
        <h1>Hello Chat!</h1>
        {!connection
            ? <Lobby joinRoom={joinRoom} isSquad={isSquad} />
            : <ChatComp sendMessage={sendMessage} messages={messages} users={users} closeConnection={closeConnection} />}
    </div>
    )

}

export default Chat;
