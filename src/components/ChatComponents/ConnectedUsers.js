const ConnectedUsers = ({ users }) => <div className='user-list'>
    <h4 style={{color: "red"}}>Connected Users:</h4>
    {users.map((u, idx) => <h6 key={idx} style={{color: "red"}}>{u}</h6>)}
</div>

export default ConnectedUsers;
