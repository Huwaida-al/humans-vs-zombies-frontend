import { useAuth0 } from "@auth0/auth0-react";
import React, { useState } from "react";
import { UserAPI } from "../../api/UserAPI";


const LoginButton = ({id }: any) => {

  const [ error, setError ] = useState<string>();


  const onClickLogin = async () => {
    try {
      const { data } = await UserAPI.get(id)
      if (data) {
        loginWithRedirect();
      }
      
    } catch (e : any) {
      setError(e.message);
    }

  }

  const { loginWithRedirect } = useAuth0();

  return (
    <button className="btn btn-primary btn-sm m-1" onClick={() => loginWithRedirect()}>
      Login
    </button> 
  )
};

export default LoginButton;