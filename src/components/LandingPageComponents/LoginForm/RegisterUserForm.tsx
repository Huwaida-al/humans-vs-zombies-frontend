import { SyntheticEvent, useState, ChangeEvent, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../../../hooks'
import { registerAttemptAction, registerResetAction } from '../../../store/actions/userActions';
import { IEditUser } from '../../../types/UserTypes';
import { useAuth0 } from "@auth0/auth0-react";
import Error from '../../../hoc/UIMessagesComponents/Error';
import Success from '../../../hoc/UIMessagesComponents/Success';

const RegisterUserForm = () => {
    const { user } = useAuth0();

    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const { registerAttempting, registerError, registerSuccess } = useAppSelector(state => state.register)

    const [credentials, setCredentials] = useState({
        firstName: "",
        lastName: ""
    });

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setCredentials({ ...credentials, [name]: value });
    };

    const onFormSubmit = async (event : SyntheticEvent )  => {
        event.preventDefault();
        if (user && user.email) {
            const registerUser : IEditUser = {
                firstName: credentials.firstName,
                lastName: credentials.lastName,
                id: user.email,
                isAdmin: false
            }
            dispatch(registerAttemptAction(registerUser));
        }
    }

    useEffect(()=>{
        if (registerSuccess) {
            setCredentials({ ...credentials, firstName: "", lastName: "" });
            dispatch(registerResetAction());
            navigate("/")
        }
    }, [registerSuccess])

    return (
        <>
            <h1 className="mb-3 text-body">Register User</h1>
            <p className="mb-3 text-body"> We seem to miss some information about you, please fill in the following: </p>
            <form onSubmit={onFormSubmit}>
                <div className="card-body p-md-5 text-black">
                    <input className='form-outline mb-4 form-control form-control-lg' type="text" value={credentials.firstName} placeholder="First name" name="firstName" onChange={handleInputChange} required minLength={2}/>
                    <input className='form-outline mb-4 form-control form-control-lg' type="text" value={credentials.lastName} placeholder="Last name" name="lastName" onChange={handleInputChange} required minLength={2}/>
                    <div className="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                        <button className='btn btn-success btn-lg' type="submit">Register</button>
                    </div>
                </div>

                
            </form>
            { registerAttempting &&
                <p>Trying to register...</p>
            }
            { registerError &&
                <Error message={registerError} />
            }
            {/* { registerSuccess &&
                <Success message={<p>Your user has been registered successfully.</p>} hide={true} action={registerResetAction()} />
            } */}
        </>

    )
}

export default RegisterUserForm;