import { useState } from "react";
import { useAppSelector } from "../../hooks";
import LogoutButton from "./LoginForm/LogoutButton";
import '../../App.css'

const UserProfile = () => {
    const [ showMessage, setShowMessage ] = useState<boolean>(false);
    const { username } = useAppSelector(state => state.session);

    const showUserProfile = () => {
        if(showMessage === true) {
            setShowMessage(false)
        } 
        if(showMessage === false) {
            setShowMessage(true)
        }
        
    }

    return (
            <div className='card-sm navbar-text text-end p-5 '>
                <div className='card-body-sm position-absolute top-0 end-0'>
                    { showMessage &&
                        <div className='m-1 right-header'>{username}</div> 
                    }
                    <div>
                        <button className="btn btn-outline-light btn-sm m-1 rounded-circle" 
                        onClick={showUserProfile}>
                            <span className="material-icons">person</span>
                        </button>
                    </div>
                    <div>
                        <LogoutButton />
                    </div>
                </div>
            </div>
    )
}

export default UserProfile