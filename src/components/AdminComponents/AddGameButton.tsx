import { Link } from "react-router-dom";
import CollapseCard from "../../hoc/CollapseCard";

const AddGameButton = () => {
    return (
        <CollapseCard title="Administrator Options:" show={true}>
                <Link to="/add"  className="btn btn-primary">Add New Game</Link>
        </CollapseCard>
    )
}

export default AddGameButton;