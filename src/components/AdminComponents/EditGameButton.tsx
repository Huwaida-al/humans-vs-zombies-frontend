import { useNavigate } from "react-router-dom";

const EditGameButton = () => {
    const navigate = useNavigate();


    const editGame = () => {
        const path = '/editgame'
        navigate(path)
    }

    return (
        <button type="submit" className="btn btn-success me-1" onClick={editGame}>Edit Game</button>
    )


}

export default EditGameButton;