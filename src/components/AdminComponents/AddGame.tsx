import React, { useState, ChangeEvent, SyntheticEvent } from "react";
import { IGame, IEditGame } from "../../types/GameTypes";
import { Link, Navigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { gameCreateAttemptAction, gameCreateCompleteAction } from "../../store/actions/gameActions";
import Success from "../../hoc/UIMessagesComponents/Success";
import Attempting from "../../hoc/UIMessagesComponents/Attempting";
import Error from "../../hoc/UIMessagesComponents/Error";
import Map from "../GamePageComponents/Map/Map";

const AddGame: React.FC = () => {

    const dispatch = useAppDispatch();
    const { createGameAttempting, createGameError, createGameSuccess } = useAppSelector(state => state.createGameReducer);

    const [position, setPosition] = useState<any | null>([60.391000, 5.327330])

    const [game, setGame] = useState<IEditGame>({
        name: "",
        gameState: "Registration",
        description: "",
        nw_Lat: 0,
        nw_Ing: 0,
        se_Lat: 0,
        se_Ing: 0
    })

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setGame({ ...game, [name]: value });
    };

    const saveGame = async (event : SyntheticEvent) => {
        event.preventDefault();
        game.se_Lat = position.lat;
        game.se_Ing = position.lng;
        dispatch(gameCreateAttemptAction(game));
    };

    return (
        <>
        <div className="card">
                <div className="card-header">
                    <div className="nav navbar-nav text-end">
                            <li><Link to="/" className="btn-close btn-close-black btn-lg me-3 text-body" aria-label="Close"></Link></li>
                    </div>
                    <h3>Add Game:</h3>
                </div>
                <div className="submit-form mt-3 card-body p-md-5 text-black">
                    <div className="submit-form">
                        <form onSubmit={saveGame}>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={game.name}
                                    onChange={handleInputChange}
                                    name="name"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="description">Description</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="description"
                                    required
                                    value={game.description}
                                    onChange={handleInputChange}
                                    name="description"
                                />
                            </div>
                            <div className="form-group">
                            <div className="h6">Please select area boundaries and map centre position for your game:</div>
                                <label htmlFor="mapwidth">Game area map width (in meters)</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="mapwidth"
                                    required
                                    value={game.nw_Ing}
                                    onChange={handleInputChange}
                                    name="nw_Ing"
                                /> 
                            </div>
                            <div className="form-group">
                                <label htmlFor="mapheight">Game area map height (in meters)</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="mapheight"
                                    required
                                    value={game.nw_Lat}
                                    onChange={handleInputChange}
                                    name="nw_Lat"
                                /> 
                            </div>
                            <div className="form-group">
                                <div>Game Map centre position</div>
                                <Map position={position} setPosition={setPosition}></Map>
                            </div>
                            <button onClick={saveGame} className="btn btn-success mt-2" type="submit">
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            
        </div>
            { createGameAttempting &&
                <Attempting message="Adding game..."/>
            }
            { createGameSuccess && 
                <Success message={"New game added: "+ game.name} path="/add" close="Add Another Game" action={ gameCreateCompleteAction() }/>
            }
            { createGameError && 
                <Error message={"Failed to create game: " + createGameError}/>
            }
        </>
    );
};
export default AddGame;
