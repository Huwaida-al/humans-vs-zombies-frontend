import { ChangeEvent, SyntheticEvent, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { gameFetchAttemptAction, gamesFetchAttemptAction, gameUpdateAttemptAction, gameUpdateCompleteAction } from "../../store/actions/gameActions";
import {IEditGame} from "../../types/GameTypes";
import Attempting from "../../hoc/UIMessagesComponents/Attempting";
import Error from "../../hoc/UIMessagesComponents/Error";
import Success from "../../hoc/UIMessagesComponents/Success";
import { loadStore } from "../../hoc/LocalStorage";
import { IEditPlayer, IPlayer } from "../../types/PlayerTypes";
import { playerInGameGetAttemptAction, playerUpdateAttemptAction, playerUpdateCompleteAction } from "../../store/actions/playerActions";
import Map from "../GamePageComponents/Map/Map";

const EditGame: React.FC = () => {

    const dispatch = useAppDispatch();
    const { allPlayers, getPlayerInGameAttempting, getPlayerInGameError} = useAppSelector(state => state.getPlayersInGame)
    const { updateGameAttempting, updateGameError, updateGameSuccess} = useAppSelector(state => state.update);
    const { updatePlayerAttempting, updatePlayerError, updatePlayerSuccess} = useAppSelector(state => state.updatePlayer);
    const {game, getOneAttempting, getOneSuccess, getOneError} = useAppSelector(state => state.game)

    // states
    const [patient0ID, setPatient0ID] = useState<number>(0);
    console.log(game);
    console.log(game?.se_Lat)
    console.log(game?.se_Ing)
    const [updatingPlayer, setUpdatingPlayer] = useState<boolean>(false);

    const startPos = () => {
        if ( game && game.se_Lat && game.se_Ing) return [game?.se_Lat, game?.se_Ing] 
        else return [60.391000, 5.327330]
    }
    const [position, setPosition] = useState<any | null>(startPos());

    const [updateGame, setUpdateGame] = useState<IEditGame>({
        name: game?.name,
        gameState: game?.gameState,
        description: game?.description,
        nw_Lat: game?.nw_Lat,
        nw_Ing: game?.nw_Ing,
        se_Lat: game?.se_Lat,
        se_Ing: game?.se_Ing
    })

    interface IGameStateOption {
        label : string,
    }
    const gameStateOption : IGameStateOption[] = [
        { label: "-- Select Game Status --" },
        { label: "Registration", },
        { label: "In Progress", },
        { label: "Complete", }
    ]

    // handle changes in form:
    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setUpdateGame({ ...updateGame, [name]: value})
    };
    const handleSelectInputChange = (event: ChangeEvent<HTMLSelectElement>) => {
        console.log(updateGame.gameState)
        const { name, value } = event.target;
        if (value !== "-- Select Game Status --") {
            setUpdateGame({ ...updateGame, [name]: value})
        }
    };
    
    const handleSelectPatient0 = (event: ChangeEvent<HTMLSelectElement>) => {
        console.log(patient0ID)
        const { value } = event.target;
        setPatient0ID(parseInt(value));
        console.log(patient0ID)
    };

    /**
     * Sends api call for players
     */
    const updatePlayers = () => {
        if (updatePlayerSuccess) {
            if (game !== null) {
                dispatch(playerInGameGetAttemptAction(game.id));
            } else {
                const gameID = loadStore("gameID")
                dispatch(gameFetchAttemptAction(gameID));
                dispatch(playerInGameGetAttemptAction(gameID));
            }
        }
    }

    const submitChangedGame = async (event : SyntheticEvent )  => {
        event.preventDefault();
        // also update player if necessary
        console.log(updateGame)
        if (patient0ID !== 0 && updateGame.gameState === "In Progress") {
            setUpdatingPlayer(true);
            console.log("went in here")
            const patient0 : IEditPlayer = {
                isHuman: false,
                isPatientZero: true
            };
            dispatch(playerUpdateAttemptAction(patient0ID, patient0))
        }
        const gameID = loadStore("gameID")
        if (position.lng || position.lat) {
            updateGame.se_Ing = position.lng;
            updateGame.se_Lat = position.lat;
        }
        dispatch(gameUpdateAttemptAction(gameID, updateGame))
    }

    const hasPatient0 = () : boolean => {
        for (const p of allPlayers) {
            if (p.isPatientZero) return true
        }
        return false
    }

    // will execute if allPlayers doesn't exist and isn't being fetched:
    useEffect(()=>{
        if (!(allPlayers.length > 0 || getPlayerInGameAttempting || getPlayerInGameError)) updatePlayers() 
    }, [allPlayers, getPlayerInGameAttempting, getPlayerInGameError]);
    
    // will execute when successfully updating game:
    useEffect(()=>{
        if (updateGameSuccess) {
            dispatch(gameFetchAttemptAction(game?.id))
        } 
    }, [updateGameSuccess]);

    //will execute when successfully updating player:
    useEffect(()=>{
        if (updatePlayerSuccess) {
            updatePlayers()
        } 
    }, [updatePlayerSuccess]);

    
    return (
        <>
            
            <div className="card">
                <div className="card-header">
                    <div className="nav navbar-nav text-end">
                            <li><Link to="/" className="btn-close btn-close-black btn-lg me-3 text-body" aria-label="Close"></Link></li>
                    </div>
                    <h3>Edit Game:</h3>
                    <h3 >{game?.name}</h3>
                </div>
                <div className="submit-form mt-3 card-body p-md-5 text-black">
                    <form onSubmit={submitChangedGame}>
                        <div className="form-group">
                            <label htmlFor="name">Name:</label>
                            <input
                                type="text"
                                placeholder="Zombie eat all humans"
                                className="form-control mb-3 mt-1"
                                value={updateGame.name}
                                id="name"
                                onChange={handleInputChange}
                                name="name"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Game Description:</label>
                            <input
                                type="text"
                                placeholder="This game is scary...."
                                className="form-control mb-3 mt-1"
                                id="description"
                                value={updateGame.description}
                                onChange={handleInputChange}
                                name="description"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="state">Game State:</label>
                            <select className="form-select mb-3 mt-1 show-menu-arrow" id="state" name={"gameState"} onChange={handleSelectInputChange} title="-- Select Game Status --">
                                {gameStateOption.map((option : IGameStateOption) => (
                                    <option key={option.label} value={option.label}> {option.label}</option>
                                ))}
                            </select>
                        </div>
                        { (updateGame?.gameState === "In Progress" && !hasPatient0()) &&
                            <div className="form-group">
                                <>
                                <label htmlFor="patient0">You must choose Patient 0 (first Zombie):</label>
                                <select className="form-select mb-3 mt-1 show-menu-arrow" name="patient0" id="patient0" onChange={handleSelectPatient0} title="-- Select Player --" required>
                                    <option>-- Select Player --</option>
                                    {allPlayers?.map((p : IPlayer) => (
                                        // Should have made this nicer with getting name of user but...
                                        <option key={p.id} value={p.id}> {p.userId}</option>
                                    ))}
                                </select>
                                </>
                            </div>
                        }
                        <div className="form-group">
                            <label htmlFor="mapwidth">Game area map width (in meters)</label>
                            <input
                                type="number"
                                className="form-control"
                                id="mapwidth"
                                value={updateGame.nw_Ing}
                                required
                                onChange={handleInputChange}
                                name="nw_Ing"
                            /> 
                        </div>
                        <div className="form-group">
                            <label htmlFor="mapheight">Game area map height (in meters) currently</label>
                            <input
                                type="number"
                                className="form-control"
                                id="mapheight"
                                value={updateGame.nw_Lat}
                                required
                                onChange={handleInputChange}
                                name="nw_Lat"
                            /> 
                        </div>
                        <div className="form-group">
                            <div>Game Map centre position</div>
                            <Map position={position} setPosition={setPosition}></Map>
                        </div>  
                        <button className="btn btn-success" type="submit">
                            Submit
                        </button>
                    </form>
                    
                </div>
            </div>
            
            { updateGameAttempting &&
                <Attempting message="Updating game..."/>
            }
            { updateGameSuccess && !updatingPlayer &&
                <Success message={"Game successfully updated"} path="/editgame" close="Edit Game" action={gameUpdateCompleteAction()} />             
            }
            { updateGameSuccess && updatePlayerSuccess &&
                <Success message={"Game and patient0 successfully updated"} path="/editgame" close="Edit Game" action={gameUpdateCompleteAction()} additionalAction={playerUpdateCompleteAction()} />             
            }
            { updateGameError &&
                <Error message= {"Failed to update game: " + updateGameError}/>
            }
            { updatePlayerError &&
                <Error message={"Failed to update player: " + updatePlayerError} />
            }
            { getOneError &&
                <Error message={"Failed to fetch game: " + getOneError} />
            }
            { getPlayerInGameError &&
                <Error message={"Failed to fetch players in game: " + getPlayerInGameError} />
            }
        </>
    );
};
export default EditGame;