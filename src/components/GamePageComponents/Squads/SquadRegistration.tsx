import { ChangeEvent, SyntheticEvent, useEffect, useState } from "react"
import { Link, useNavigate } from "react-router-dom";
import Attempting from "../../../hoc/UIMessagesComponents/Attempting";
import Success from "../../../hoc/UIMessagesComponents/Success";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { ICreateSquad, SquadState } from "../../../types/SquadTypes"
import Error from "../../../hoc/UIMessagesComponents/Error";
import { squadFetchByPlayerIdAttemptingAction, squadsCreateAttemptingAction, squadsCreateCompleteAction } from "../../../store/actions/squadActions";
import SquadList from "./SquadList";
import CollapseCard from "../../../hoc/CollapseCard";
import { loadStore } from "../../../hoc/LocalStorage";



const SquadRegistration = () => {
    
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const {isHuman, playerId} = useAppSelector(state => state.playerSessionReducer)
    const {createSquadAttempting, createSquadError, createSquadSuccess} = useAppSelector(state => state.createsquad)
    const gameID = loadStore("gameID");

    
    /**
     * Epmty squad object 
     * -- used to store varibels to the new squad created 
     */
    const [createSquad, setCreateSquad] = useState<ICreateSquad>({
        name: "",
        isHuman: isHuman,
        gameId: gameID
    })

    /**
     * Handle input from text input and set to squad
     */
    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setCreateSquad({ ...createSquad, [name]: value})
    };

    /**
     * Submit create new squad form
     */
    const submitChangedGame = async (event : SyntheticEvent )  => {
        event.preventDefault();
        dispatch ( squadsCreateAttemptingAction(createSquad, playerId) )
    }

    const onClickSquads = () => {
        navigate("/squads")
    }

    useEffect(()=> {
        if (createSquadSuccess) {
            dispatch(squadFetchByPlayerIdAttemptingAction(playerId));
        }
    }, [createSquadSuccess])

    return (
        <>
            <CollapseCard title="Register Squad">
                    <div className=" form mt-3 p-md-5 text-black  ">
                        <form onSubmit={submitChangedGame}>
                            <div className="form-group form-group-sm">
                                <label htmlFor="name">Name:</label>
                                <input
                                    type="text"
                                    placeholder="The Coolest Awesome Squad"
                                    className="form-control text-center mb-2"
                                    id="name"
                                    onChange={handleInputChange}
                                    name="name"
                                />
                                {/* <label htmlFor="squadstate" >Squad State:</label>
                                <select className="form-select mb-2 show-menu-arrow text-center" name="isHuman" id="isHuman" onChange={handleSelectInputChange} title="-- Select Game Status --">
                                    {squadStateOption.map((option) => (
                                        <option key={option.value} value={option.value}> {option.value}</option>
                                    ))}
                                </select> */}
                                
                                
                            </div>
                            <button className="btn btn-success" type="submit">
                                Submit
                            </button>
                        </form>
                        { createSquadAttempting &&
                            <Attempting message={"Creating squad..."}/>
                        }
                        { createSquadError &&
                            <Error message={"Failed to create squad: " + createSquadError}/>
                        } 
                        <div className="mt-3">
                            <button onClick={onClickSquads} className="btn btn-outline-primary w-100 fw-bold text-body">Join Existing Squad</button>
                            
                        </div>
                        
                    </div>
            </CollapseCard>
        </>
    );
}

export default SquadRegistration