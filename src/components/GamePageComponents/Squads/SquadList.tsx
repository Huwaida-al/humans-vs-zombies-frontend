import { useEffect, useState } from "react";
import CollapseCard from "../../../hoc/CollapseCard";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { ISquad } from "../../../types/SquadTypes";
import SquadListItem from "./SquadListItem";
import Error from "../../../hoc/UIMessagesComponents/Error";
import { killedMembersFetchByIdAttemptingAction, squadsInGameFetchByIdAttemptingAction } from "../../../store/actions/squadActions";
import { squadMemberCreateCompleteAction } from "../../../store/actions/squadMemberActions";
import NavigationBar from "../../../hoc/NavigationBar";
import { Link } from "react-router-dom";
import Success from "../../../hoc/UIMessagesComponents/Success";

const SquadList = () => {

    //Get all players from session
    const {squads, getAllSquadssAttempting, getAllSquadssError, getAllSquadssSuccess} = useAppSelector(state => state.allSquadsInGame)
    const { createSquadMemberSuccess } = useAppSelector(state => state.createSquadMemberReducer)
    const { game } = useAppSelector(state => state.game)
    const { isHuman } = useAppSelector(state => state.playerSessionReducer)
    const dispatch = useAppDispatch();
    console.log(squads?.length)


    const getCorrectSquads = () : ISquad[] => {
        const squadList : ISquad[] = []
        for (const squad of squads) {
            if (isHuman) {
                if (squad.isHuman) {
                    squadList.push(squad)
                }
            } else {
                if (!squad.isHuman) {
                    squadList.push(squad)
                }
            }
        }
        return squadList;

    }

    const correctSquadList = getCorrectSquads();

    useEffect(() => {
        dispatch(squadsInGameFetchByIdAttemptingAction(game?.id))
    }, []);

    return (
        <>
            {/* Attempting to get all players, loading message */}
            { getAllSquadssAttempting &&
                <p>loading players...</p>

            }
            {/* Error message, if attempt fails */}
            { getAllSquadssError &&
                <Error message={getAllSquadssError} />
            } 
            {/* sends data to PlayerListItem for all players in the game, where all players is displayed  killed={killedMembersFetchByIdAttemptingAction(squads.id)} */}
            { (getAllSquadssSuccess && correctSquadList?.length < 1 && 
                <div>There are no squads for you to join in this game</div>) ||
                correctSquadList?.map((squads : ISquad) => (
                    <div key={squads.id}>
                        <SquadListItem id={squads.id} name={squads.name} squadMembers={squads.squadMembers} />
                    </div>
                ))
            }
            { createSquadMemberSuccess &&
                <Success message="Joined squad successfully" path="/game" close="Back to Game" action={squadMemberCreateCompleteAction()}/>         
            }
        </>
    )
}

export default SquadList;