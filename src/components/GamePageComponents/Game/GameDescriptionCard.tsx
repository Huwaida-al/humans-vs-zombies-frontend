import CollapseCard from "../../../hoc/CollapseCard";

const GameDescriptionCard = ({description} : any) => {

    return (
        <>
        <CollapseCard title="Description" show={true}>
            <div className="card-text"> {description} </div>
        </CollapseCard>
        </>
        
    )
}

export default GameDescriptionCard;