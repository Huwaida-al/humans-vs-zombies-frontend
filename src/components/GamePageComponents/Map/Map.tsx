import { ReactChild, ReactFragment, ReactPortal, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from "../../../hooks";
import './Map.css'
import { MapContainer, TileLayer, Marker, Popup, useMapEvents, useMap, Rectangle } from 'react-leaflet'
import { missionsFetchByGameIdAttemptAction } from '../../../store/actions/missionActions';
import { IMission } from '../../../types/MissionTypes';
import { IGame } from '../../../types/GameTypes';

// Parent send curPosition down to map.
interface Props {
    position: any;
    setPosition: (pos: any) => void;
}
export const Map = ({ position, setPosition }: Props) => {
    const dispatch = useAppDispatch();
    // Check if done with api calls
    const { getAllMissionByGameIdSuccess, gameMissions } = useAppSelector(state => state.allMissionsByGameId);
    const { createMissionSuccess } = useAppSelector(state => state.createMissionReducer);
    const { deleteMissionSuccess } = useAppSelector(state => state.deleteMission);
    const { game } = useAppSelector(state => state.game)
    // Is human or zombie
    const { isHuman } = useAppSelector(state => state.playerSessionReducer)

    // Admin/Role
    const { admin } = useAppSelector(state => state.session);
    // Current game
    const [currentGame, currentGameOnChange] = useState<IGame>();
    // Mission size. 5 meter * 5 meter
    // 1 mil. divide on number to get 1 meter. * number of meter. = 5 meter
    const mapSizeX = 1/ 111044.46 * 5
    const mapSizeY = mapSizeX *2
    // Check if page has reloaded.
    const [pageReloadDone, onStartOnChange] = useState(true);
    // All mission for current game
    const [allMissions, setAllMission] = useState<IMission[] | any>()

    // When we have all mission in current game. Run func
    useEffect(OnGetMissionSuccess, [getAllMissionByGameIdSuccess]);
    // On start get all missions and set map view on page reload
    function OnGetMissionSuccess(){
        setAllMission(gameMissions)
    }
    // Get all missions from current game
    function GetAllMissions(){
        let data = localStorage.getItem("gameID")
        if(data){
            data = JSON.parse(data)
            const gameId: number = Number(data)
            dispatch(missionsFetchByGameIdAttemptAction(gameId))
        }
    }
    
        
    // when done with deleting mission. Run get missions
    useEffect(GetAllMissions, [deleteMissionSuccess]);
    // when done with create mission. Run get missions
    useEffect(GetAllMissions, [createMissionSuccess]);

    // On page reload. Move view to game position and get current game
    const ShowGameArea = () => {
        const map = useMap()
        
        useEffect(() => {
            if(pageReloadDone === true && game ){
                onStartOnChange(false)
                currentGameOnChange(game)
                setTimeout(() => {
                    map.flyTo([game.se_Lat, game.se_Ing], 14, {
                        duration: 1
                    })
                }, 500)
            }
            return 
            
        }, [map]);
        // 69 mil = 1 meter. for game area
        const mapSizeX = 111044.46
        const mapSizeY = mapSizeX /2

        return currentGame != null ?
             <Rectangle  bounds={[
                 // Set game area
                [currentGame.se_Lat + (currentGame.nw_Lat/mapSizeX),
                    currentGame.se_Ing + (currentGame.nw_Ing/mapSizeY)],
                [currentGame.se_Lat - (currentGame.nw_Lat/mapSizeX),
                    currentGame.se_Ing - (currentGame.nw_Ing/mapSizeY)]
            ]}  ></Rectangle>
        :  null
    }

    // Click on map, add mission marker to map view
    function LocationMarker() {
        const map = useMapEvents({
          click(e) {
            map.locate()
            // Map marker position
            setPosition( e.latlng)
            // Center map view to this
            map.flyTo(e.latlng, map.getZoom())
          },
        })
        return position === null ? null : (
          <Marker position={position}>
            <Popup> New Mission</Popup>
          </Marker>
        )
      }
    return (
        <>
        {/* Leaflet map */}
        <MapContainer  center={[60.391000, 5.327330]} zoom={18} >
            {/* Init map variable and Show game area */}
            <ShowGameArea />
            {/* Select position */}
            <LocationMarker />
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            
           
            {allMissions && allMissions.length > 0 &&
                allMissions.map((marker: { id: number; lat: number; ing: number;  isHumanVisible: boolean; isZombieVisible: boolean; 
                    properties: { label: boolean | ReactChild | ReactFragment | ReactPortal | null | undefined; }; }, 
                    index: any,  ) => (
                    
                    {  /* Do nothing */}
                ) 
                && 
                // Show missions if correct role: e.g. admin, isHuman, IsZombie
                (admin || (isHuman && marker.isHumanVisible) || (!isHuman && marker.isZombieVisible) || 
                (marker.isZombieVisible && marker.isHumanVisible)) &&
                // Show missions area
                    <Rectangle  key={marker.id}  color='red'  bounds={[
                        [marker.lat + mapSizeX,
                        marker.ing + mapSizeY],
                        [marker.lat - mapSizeX,
                        marker.ing - mapSizeY]
                    ]}  
                    ></Rectangle>
                )
            }
        </MapContainer>
        </>
    )
}

export default Map