import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { playerSessionClearAction } from "../../../store/actions/sessionActions";
import Bite from "./Bite"
import { playerDeleteAttemptAction, playerGetAttemptAction} from "../../../store/actions/playerActions";
import { useNavigate } from "react-router-dom";
import { IPlayer } from "../../../types/PlayerTypes";
import CollapseCard from "../../../hoc/CollapseCard";
import { gameFetchAttemptAction, gamesFetchAttemptAction } from "../../../store/actions/gameActions";
import { IGame } from "../../../types/GameTypes";

const PlayerCard = () => {
    const dispatch = useAppDispatch();
    const { playerId, isHuman, userId, biteCode, isPatientZero} = useAppSelector(state => state.playerSessionReducer)
    const { deletePlayerSuccess } = useAppSelector(state => state.deletePlayer)
    const playerStatus = () => {
        if (isHuman) return "Human"
        else return "Zombie"
    }

    
    const onQuitGame = () => {
        // Delete player
        dispatch(playerDeleteAttemptAction(playerId))
        
    }

    return (
        <>
        <CollapseCard title="Your Stats">
            <div className="card-text">Username/email address: {userId}</div>
                    <div className="card-text">Status: {playerStatus()}</div>
                    { isPatientZero &&
                        <div className="card-text">You are Zombie No. 1!</div>
                    }
                    { isHuman && 
                        <h4>BITE CODE: {biteCode}</h4>
                    }
                    { !isHuman &&
                        <Bite />
                    }
                    <button onClick={onQuitGame} className="btn btn-danger">
                        Quit Game
                    </button>
        </CollapseCard>
        </>

    )
}

export default PlayerCard