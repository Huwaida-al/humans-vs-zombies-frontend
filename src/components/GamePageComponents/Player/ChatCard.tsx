import { Link, useNavigate } from "react-router-dom";
import CollapseCard from "../../../hoc/CollapseCard";
import Chat from "../../ChatComponents/Chat";
import "../../ChatComponents/Chat.css"

const ChatCard = ({isSquad, title} : any) => {

    const thisTitle = () => {
        if (title) {
            return title;
        }
        else {
            return "Game Chat"
        }
    }

    return (
        <CollapseCard title={thisTitle()}>
            <div className="scroll">
                <Chat isSquad={isSquad} />
            </div>
        </CollapseCard>
    )
}

export default ChatCard;