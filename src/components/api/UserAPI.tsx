import http from "../../http-common";
//import { LoginCredentials, RegisterCredentials, User } from "../../types/UserTypes"
import { IEditUser,  IUser  } from "../../types/UserTypes";

export const UserAPI = {

    async getAll() {
        return http.get<Array<IUser>>("/user");
    },
    async get(id: any) {
        return http.get<IUser>(`/user/${id}`);
    },
    async create(data: IEditUser) {
        data.isAdmin = false;
        return http.post<IUser>("/user", data);
    },
    async update(id: any, data: IEditUser) {
        return http.put<any>(`/user/${id}`, data);
    },
    async remove(id: any) {
        return http.delete<any>(`/user${id}`);
    },
    async removeAll() {
        return http.delete<any>(`/user`);
    },
    // get players by user, but does it work??
    async getUserPlayers(id: any) {
        return http.get<Array<IUser>>(`/user/${id}/players`);
    },

}