import http from "../../http-common";
import { ISquadMember, ICreateSquadMember } from "../../types/SquadMemberTypes";

const getAll = () => {
    return http.get<Array<ISquadMember>>("/squadmember");
};
const get = (id: any) => {
    return http.get<ISquadMember>(`/squadmember/${id}`);
};
const create = (data: ICreateSquadMember) => {
    return http.post<ISquadMember>("/squadMember", data);
};
const update = (id: any, data: ICreateSquadMember) => {
    return http.put<any>(`/squadmember/${id}`, data);
};
const remove = (id: any) => {
    return http.delete<any>(`/squadmember/${id}`);
};
const removeAll = () => {
    return http.delete<any>(`/squadmember`);
};

const SquadMemberAPI = {
    getAll,
    get,
    create,
    update,
    remove,
    removeAll
};
export default SquadMemberAPI

