import http from "../../http-common";
import { RegisterPlayerCredentials, IPlayer, IEditPlayer } from "../../types/PlayerTypes";

const getAllPlayers = () => {
    return http.get<Array<IPlayer>>("/Player");
};
const get = (id: any) => {
    return http.get<IPlayer>(`/player/${id}`);
};
// const getByUserId = (id: any) => {
//     return http.get<Player>(`player/${id}`)
// };
const create = (data: RegisterPlayerCredentials) => {
    return http.post<IPlayer>("/player", data);
};
const update = (id: any, data: IEditPlayer) => {
    return http.put<any>(`/player/${id}`, data);
};
const remove = (id: any) => {
    return http.delete<any>(`/player/${id}`);
};
const removeAll = () => {
    return http.delete<any>(`/Player`);
};
const getByBiteCode = (biteCode : string) => {
    return http.get<any>(`/Player/${biteCode}/getByBiteCode`);
}

const getAllPlayersInGame = (gameId : number) => {
    return http.get<any>(`/Player/${gameId}/getbyGameId`);
}

const PlayerService= {
    get,
    getAllPlayers,
    create,
    update,
    remove,
    removeAll,
    getByBiteCode,
    getAllPlayersInGame
};

export default PlayerService
