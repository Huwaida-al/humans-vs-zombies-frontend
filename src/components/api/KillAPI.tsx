import http from "../../http-common";
import { ICreateKill, IRegisterKill } from "../../types/KillTypes";

const create = (data: ICreateKill) => {
    return http.post<ICreateKill>("/kill", data);
};

const registerKillAndUpdatePlayer = (data : IRegisterKill) => {
    return http.post(`/Kill/${data.gameId}/kill`, data);
}

const KillAPI = {
    create,
    registerKillAndUpdatePlayer
}

export default KillAPI;