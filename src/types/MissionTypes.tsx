export interface IMission {
    id: any | null,
    name: string | null,
    description: string | null,
    isHumanVisible: boolean,
    isZombieVisible: boolean,
    startTime: Date,
    dateTime: Date,
    lat: number,
    ing: number,
    gameId: number,
}

export interface IEditMission {
    name: string,
    description: string,
    isHumanVisible: boolean,
    isZombieVisible: boolean,
    startTime: Date,
    dateTime: Date,
    lat: number,
    ing: number,
    gameId: number
}

