export interface ISquadMember {
    id: any | null,
    rank: number,
    gameId: number,
    squadId: number,
    playerId: number
}

export interface ICreateSquadMember {
    rank: number,
    gameId: number,
    squadId: number,
    playerId: number
}

export interface IUpdateSquadMember {
    rank: number,
    gameId: number,
    squadId: number,
    playerId: number
}
