export interface IGame {
    id: any | null,
    name: string,
    gameState: string,
    players: number[] | null,
    description: string
    nw_Lat: number,
    nw_Ing: number,
    se_Lat: number,
    se_Ing: number,
}

export interface IEditGame {
    name: string,
    gameState: string,
    description: string,
    nw_Lat: number,
    nw_Ing: number,
    se_Lat: number,
    se_Ing: number,
}

export interface FindPlayers {
    id: number
}

