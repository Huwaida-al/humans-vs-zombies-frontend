export interface IUser {
    id: string,
    firstName: string,
    lastName: string,
    isAdmin: boolean,
    players: number[]
}

export interface IKill {
    //id: number,
    gameId: number,
    killerId: number,
    victimId: number,
    // description
}

export interface IEditUser {
    id: string,
    firstName: string,
    lastName: string,
    isAdmin: boolean
}