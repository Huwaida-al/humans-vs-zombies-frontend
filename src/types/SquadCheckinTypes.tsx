export interface ISquadCheckin {
    id: any | null,
    gameId: number,
    squadId: number,
    squadMemberId: number 
}

export interface ICreateSquadCheckin {
    gameId: number,
    squadId: number,
    squadMemberId: number
}

export interface IUpdateSquadCheckin {
    gameId: number,
    squadId: number,
    squadMemberId: number
}
