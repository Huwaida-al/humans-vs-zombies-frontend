export interface IKill {
    id: number,
    gameId : number,
    killerId : number,
    victimId : number
}

export interface ICreateKill {
    gameId : number,
    killerId : number,
    victimId : number
}

export interface IRegisterKill {
    gameId : number,
    killerId : number,
    biteCode : string
}