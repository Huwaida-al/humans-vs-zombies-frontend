import React from 'react';
import './App.css';
import LandingPage from './pages/LandingPage/LandingPage'
import GamePage from './pages/GamePage/GamePage'
import NotFound from './pages/NotFound/NotFound'
import CreateGamePage from './pages/LandingPage/CreateGamePage';
import EditGamePage from './pages/GamePage/EditGamePage';
import Success from './hoc/UIMessagesComponents/Success';
import NoAccessPage from './pages/NotAccessible/NoAccessPage';
import RegisterUserPage from './pages/LandingPage/RegisterUserPage';
import EditPlayersPage from './pages/GamePage/EditPlayersPage';
import AppContainer from './hoc/AppContainer';
import SquadListPage from './pages/GamePage/SquadListPage';
import ChatPage from './pages/GamePage/ChatPage';

import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppContainer>
        {/* Here goes the routes (bite, add can be removed) */}
        <Routes>
            <Route path="/" element={ < LandingPage /> } />
            <Route path="/register" element={ < RegisterUserPage /> } />
            <Route path="/game" element={ < GamePage /> } />
            <Route path ="/chat" element={ < ChatPage /> } /> 
            <Route path="/add" element={ < CreateGamePage />} />
            <Route path="/editgame" element={ < EditGamePage />} />
            <Route path='/editplayers' element={ < EditPlayersPage />} />
            <Route path='/squads' element={ < SquadListPage />} />
            <Route path="/success" element={ < Success />} />
            <Route path="/notaccessible" element={ < NoAccessPage/>} />
            <Route path="*" element={ < NotFound />} />
          </Routes>

        </AppContainer>
      </div>
    </BrowserRouter>
  );
}

export default App;
