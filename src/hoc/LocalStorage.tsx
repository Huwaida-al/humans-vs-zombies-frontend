/**
 * load a value from local storage
 * @param name name of value in local storage
 * @returns parsed value if it exists, undefined if not
 */
export const loadStore = (name : string) : any => {
    const item = localStorage.getItem(name);
    if (!item) return undefined;
    else return JSON.parse(item);
}

/**
 * Save a value in local storage.
 */
export const saveStore = (name: string, value : any) : void => {
    const item = JSON.stringify(value);
    localStorage.setItem(name, item);
}