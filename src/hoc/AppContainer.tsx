import './AppContainer.css';

export const AppContainer = ({ children }: any) => {
    return (
        <div className="container AppContainer">{ children }</div>
    )
}
export default AppContainer
