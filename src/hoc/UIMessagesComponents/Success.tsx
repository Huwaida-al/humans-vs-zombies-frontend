import { Alert } from "react-bootstrap"
import { Link, Navigate, useNavigate} from "react-router-dom"
import './Success.css'
import { useAppDispatch } from "../../hooks"

const Success = ({message, path, close, hide, action, additionalAction, home, homeName}: any) => {
    const navigate = useNavigate()
    const dispatch = useAppDispatch()

    const navigateToCurrentPage = () => {
        dispatch(action);
        if (additionalAction) {
            dispatch(additionalAction)
        }
        // window.location.reload();
        navigate(path)
    }

    const navigateToHome = () => {
        dispatch(action);
        if (additionalAction) {
            dispatch(additionalAction)
        }
        if (home) {
            navigate(home)
        }
        else {
            navigate("/");
        }
    }

    const homeDescription = () : string => {
        if (homeName) return homeName
        else return "Home" 
    }
 
    return (
            <Alert className="alert alert-success fixed-top success-message-container">
                <div className="card center "> 
                    <h5 className="card-title mt-3">Congratulations!</h5>
                    <div className="card-body">
                        <div>{message}</div>
                        { !hide &&
                        <button type="button" className="btn btn-sm btn-secondary m-1" onClick={navigateToCurrentPage}>{close}</button>
                        }
                        <button type="button" className="btn btn-sm btn-primary m-1" onClick={navigateToHome}>{homeDescription()}</button>
                    </div> 
                </div>
            </Alert>
        )
}

export default Success