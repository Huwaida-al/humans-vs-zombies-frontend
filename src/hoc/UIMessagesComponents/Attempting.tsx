

const Attempting = ({message}: any) => {
    return (
        <div className='alert alert-info m-4 p-1 d-inline'>
                <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span className="sr-only"><h5>{message}</h5></span>
        </div>
    )
    

}

export default Attempting