import axios from "axios";
export const DeployURL  = "https://appservicebackend.azurewebsites.net/";
//export const DeployURL  = "https://localhost:44397/";
export default axios.create({
    baseURL: DeployURL + "api",

    headers: {
        "Content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        //"Access-Control-Allow-Credentials": "true"
    }
});