import { Link, Navigate } from "react-router-dom";
import AddGame from "../../components/AdminComponents/AddGame";
import LoginCheck from "../../hoc/CheckLogin";
import NavigationBar from "../../hoc/NavigationBar";
import { useAppSelector } from "../../hooks";

const CreateGamePage = () => {
    const { admin } = useAppSelector(state => state.session);
    return (
        <main>
            { (admin && 
                <>
                    <NavigationBar>
                    </NavigationBar>
                    <AddGame />
                </>) ||
                <Navigate to ="/" />
            }
        </main>
    )
}

export default CreateGamePage;