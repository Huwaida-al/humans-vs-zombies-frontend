import LoginForm from "../../components/LandingPageComponents/LoginForm/LoginForm";
import NavigationBar from "../../hoc/NavigationBar";
import GameList from "../../components/LandingPageComponents/GameList/GameList";
import AddGameButton from "../../components/AdminComponents/AddGameButton";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../hooks";
import UserProfile from "../../components/LandingPageComponents/UserProfile";
import { playerSessionClearAction } from "../../store/actions/sessionActions";
import { gameFetchCompleteAction } from "../../store/actions/gameActions";
import { missionsFetchByGameIdCompleteAction } from "../../store/actions/missionActions";
import { squadFetchByPlayerIdCompleteAction, squadMemberInSquadFetchByIdCompleteAction, squadsInGameFetchByIdCompleteAction } from "../../store/actions/squadActions";
import { Link } from "react-router-dom";

const LandingPage = () => {
    const { admin, loggedIn } = useAppSelector(state => state.session);
    const dispatch = useAppDispatch();
    
    useEffect(()=> {
        localStorage.removeItem('gameID');
        dispatch(playerSessionClearAction());
        dispatch(gameFetchCompleteAction());
        dispatch(missionsFetchByGameIdCompleteAction());
        dispatch(squadsInGameFetchByIdCompleteAction());
        dispatch(squadFetchByPlayerIdCompleteAction());
        dispatch(squadMemberInSquadFetchByIdCompleteAction());

    }, [])

    return (
        
        <main>
            <NavigationBar>
                { !loggedIn && <LoginForm/>}
                { loggedIn && <UserProfile/>
            }
            </NavigationBar>
            { admin &&
                <AddGameButton />
            }
            <GameList />
            { loggedIn && 
                <Link to="/chat" className="btn btn-success">Go to ChatRoom!</Link>
            }
        </main>
    )
}

export default LandingPage;