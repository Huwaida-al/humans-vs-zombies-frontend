import {Link} from 'react-router-dom';

const NoAccessPage = () => {
    return (
        <main>
            <h4>Hey</h4>
            <p>Unfortunately, you do not have the rights to access this page.</p>
            <Link to="/">Take me home</Link>
        </main>
    )
}

export default NoAccessPage;