import NavigationBar from "../../hoc/NavigationBar";
import PlayerList from "../../components/AdminComponents/Playerlist"
import { Link, Navigate } from "react-router-dom";
import { useAppSelector } from "../../hooks";

const EditPlayersPage = () => {
    const { admin } = useAppSelector(state => state.session);

    return (
        <main>
            { (admin && 
                <>
                    <NavigationBar>
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/game" className="btn-close btn-close-white" aria-label="Close"></Link></li>
                        </ul>
                    </NavigationBar>
                    <PlayerList />
                </>) ||
                <Navigate to ="/" />
            }
            
        </main>
    )
}

export default EditPlayersPage;