import { Link } from "react-router-dom";
import SquadList from "../../components/GamePageComponents/Squads/SquadList";
import LoginCheck from "../../hoc/CheckLogin";
import NavigationBar from "../../hoc/NavigationBar";
import { useAppSelector } from "../../hooks";

const SquadListPage = () => {
    const { game } = useAppSelector(state => state.game)
    return (
        <>
        <LoginCheck />
        <NavigationBar>
            <ul className="nav navbar-nav navbar-right">
                    <li><Link to="/game"  className="btn-close btn-close-white" aria-label="Close"></Link></li>
            </ul>
            <div className='container'>
                <h2>{ game?.name }</h2>
            </div>
        </NavigationBar>
        <SquadList />
        </>

    )
}

export default SquadListPage;