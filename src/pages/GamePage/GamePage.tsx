import {Link, Navigate, useNavigate} from 'react-router-dom';
import { useEffect, useState } from 'react';
import AdminGameOptions from '../../components/AdminComponents/AdminGameOptions';
import GameDescriptionCard from '../../components/GamePageComponents/Game/GameDescriptionCard';
import GameRulesCard from '../../components/GamePageComponents/Game/GameRulesCard';
import PlayerCard from '../../components/GamePageComponents/Player/PlayerCard';
import NavigationBar from '../../hoc/NavigationBar';
import '../../App.css'

import { useAppDispatch, useAppSelector } from "../../hooks";
import { gameFetchAttemptAction, gameFetchCompleteAction, gamesFetchAttemptAction } from '../../store/actions/gameActions';
import JoinGame from '../../components/GamePageComponents/Player/JoinGame';
import { playerCreateCompleteAction, playerGetAttemptAction } from '../../store/actions/playerActions';
import { playerSessionClearAction } from '../../store/actions/sessionActions';
import { useAuth0 } from '@auth0/auth0-react';
import LoginCheck from '../../hoc/CheckLogin';
import ChatCard from '../../components/GamePageComponents/Player/ChatCard';
import { IPlayer } from '../../types/PlayerTypes';
import Error from '../../hoc/UIMessagesComponents/Error';
import Success from '../../hoc/UIMessagesComponents/Success';
import { squadFetchByPlayerIdAttemptingAction, squadFetchByPlayerIdCompleteAction, squadMemberInSquadFetchByIdCompleteAction, squadsCreateCompleteAction, squadsFetchAttemptingAction, squadsInGameFetchByIdAttemptingAction, squadsInGameFetchByIdCompleteAction } from '../../store/actions/squadActions';
import SquadRegistration from '../../components/GamePageComponents/Squads/SquadRegistration';
import MapSubmit from '../../components/GamePageComponents/Map/MapSubmit';
import { missionsFetchByGameIdAttemptAction, missionsFetchByGameIdCompleteAction } from '../../store/actions/missionActions';
import { loadStore } from '../../hoc/LocalStorage';
import PlayerSquadCard from '../../components/GamePageComponents/Squads/PlayerSquadCard';



// leaflet needs css for displaying correctly 
const GamingPage = () => {

    const dispatch = useAppDispatch();
    const {  getOneAttempting, getOneError, getOneSuccess, game } = useAppSelector(state => state.game);
    const { createPlayerSuccess } = useAppSelector(state => state.createPlayerReducer);
    const { playerId, isHuman } = useAppSelector(state => state.playerSessionReducer)
    const { admin, players, username } = useAppSelector(state => state.session);
    const {  squad  } = useAppSelector(state => state.getSquadByPlayerReducer)
    const { createSquadSuccess} = useAppSelector(state => state.createsquad)
    
    /**
     * Actions to perform when game has loaded:
     *  check if user is player in this game, and dispatch playerGetAttemptAction if so.
     */
    const onLoadedGame = () => {
        if (getOneSuccess) {
            findPlayer();
        }
    }

    /**
     * Checks if user is player in the game, and dispatch action to get player if so.
     */
    const findPlayer = () => {
        if (players.length > 0 && game && (game.players !== null)) {
            // find intersection between the user's players and the players in the game
            const intersection = game.players.filter((p : IPlayer) => players.includes(p));
            // Check if user is player in the game:
            if (intersection.length > 0) {
                // get player and set playersession:
                dispatch(playerGetAttemptAction(intersection[0]));
                // get squad player is member of (if any)
                dispatch(squadFetchByPlayerIdAttemptingAction(intersection[0]));
            }
        }
    }

    // will execute when game is set
    useEffect(onLoadedGame, [getOneSuccess]);

    const onExit = () => {
        localStorage.removeItem("gameID");
        dispatch(playerSessionClearAction());
        dispatch(gameFetchCompleteAction());
        dispatch(missionsFetchByGameIdCompleteAction());
        dispatch(squadsInGameFetchByIdCompleteAction());
        dispatch(squadFetchByPlayerIdCompleteAction());
        dispatch(squadMemberInSquadFetchByIdCompleteAction());
        dispatch(squadsCreateCompleteAction());
    }

    const refresh = () => {
        const gameID = loadStore("gameID");
        dispatch(gameFetchAttemptAction(gameID));
        dispatch(squadsInGameFetchByIdAttemptingAction(gameID));
        dispatch(missionsFetchByGameIdAttemptAction(gameID));
    }



    return (
        <main>
            
            <LoginCheck />
            
            <>
                <NavigationBar>
                    <ul className="nav navbar-nav navbar-right">
                        <div className='button-group'>
                        <div onClick={refresh} className="btn refresh-button m-0 p-1 mt-1 mx-2 btn-outline-light btn-large shadow-none" aria-label="Refresh">
                            <span className="material-icons"> refresh </span></div>
                        <Link to="/" onClick={onExit} className="btn btn-large btn-close btn-close-white" aria-label="Close"></Link>
                        </div>
                    </ul>
                    <div className='container'>
                        <h2>{ game?.name }</h2>
                    </div>
                </NavigationBar>
                <>
                    
                    <div >
                        { admin && <AdminGameOptions />
                        }
                        <GameDescriptionCard description={game?.description} />
                        <GameRulesCard />

                        { playerId && 
                            <>
                            <PlayerCard />
                            { (!squad) &&
                                <SquadRegistration />
                            }
                            { squad && 
                                <PlayerSquadCard /> 
                            }
                            <ChatCard isSquad={false} />
                            </>
                        }

                        { (playerId || admin) &&
                            <MapSubmit></MapSubmit>
                        }

                        { !admin && !playerId && game?.gameState === "Registration" &&
                                <JoinGame userID={username}  gameID={game?.id} />
                        }
                        
                    </div>
                    
                </>
                { getOneError && 
                    <Error message={getOneError} />
                }
                { getOneAttempting &&
                    <p>loading game...</p>
                }
                { createPlayerSuccess && 
                    <Success message={<p>Joined game successfully</p>} path="/game" close="Game Page" action={playerCreateCompleteAction()} />
                 }
                { createSquadSuccess &&
                    <Success message={<p>Squad successfully created</p>} path="/game" close="Close" action={squadsCreateCompleteAction()}/>             
                }
            
            </>
        </main>
    )
}

export default GamingPage;