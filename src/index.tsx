import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './store';
import { Auth0Provider } from "@auth0/auth0-react";
import { PersistGate } from 'redux-persist/integration/react'

ReactDOM.render(
  <Auth0Provider
    domain="dev-eb4ofg0l.us.auth0.com"
    clientId="6jLDn3fti9ZKal3obzmrkBpGQtKKOeVJ"
    redirectUri={window.location.origin}
    audience="https://hvsz"
    scope="create:game update:game read:game delete:game">
    <React.StrictMode>
      <Provider store = { store.store }>
        <PersistGate loading={null} persistor={store.persistor}>
          <App />
        </PersistGate>
      </Provider>
    </React.StrictMode>
  </Auth0Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
