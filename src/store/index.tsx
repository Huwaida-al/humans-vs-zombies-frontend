import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import { encryptTransform } from 'redux-persist-transform-encrypt';
import storage from 'redux-persist/lib/storage'
import { composeWithDevTools } from "redux-devtools-extension";
import appReducer from "./reducers"; //because we've called the files "index.js", we don't have to
import middleware from "./middleware"; // specify the import in more detail

const persistConfig = {
    key: 'root',
    storage,
    transforms: [
        encryptTransform({
            secretKey: 'my-super-secret-key',
            onError: function (error) {
              console.log(error.message);
            },
        }),
    ],
}

const persistedReducer = persistReducer(persistConfig, appReducer);

// Store with all bundles

const store = createStore(
    persistedReducer,
    // appReducer,
    composeWithDevTools(middleware),
)

const persistor = persistStore(store);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch


// eslint-disable-next-line import/no-anonymous-default-export
export default {store, persistor};
// export default store;