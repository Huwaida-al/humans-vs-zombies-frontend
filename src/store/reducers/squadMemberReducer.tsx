import { AnyAction } from "redux";
import { ACTION_SQUAD_GETBYPLAYERID_ATTEMPTING, ACTION_SQUAD_GETBYPLAYERID_COMPLETE, ACTION_SQUAD_GETBYPLAYERID_ERROR, ACTION_SQUAD_GETBYPLAYERID_SUCCESS } from "../actions/squadActions";
import { ACTION_SQUADMEMBER_CREATE_ATTEMPTING, ACTION_SQUADMEMBER_CREATE_COMPLETE, ACTION_SQUADMEMBER_CREATE_ERROR, ACTION_SQUADMEMBER_CREATE_SUCCESS, ACTION_SQUADMEMBER_DELETE_ATTEMPTING, ACTION_SQUADMEMBER_DELETE_COMPLETE, ACTION_SQUADMEMBER_DELETE_ERROR, ACTION_SQUADMEMBER_DELETE_SUCCESS, ACTION_SQUADMEMBER_GETALL_ATTEMPTING, ACTION_SQUADMEMBER_GETALL_COMPLETE, ACTION_SQUADMEMBER_GETALL_ERROR, ACTION_SQUADMEMBER_GETALL_SUCCESS, ACTION_SQUADMEMBER_GETBYID_ATTEMPTING, ACTION_SQUADMEMBER_GETBYID_COMPLETE, ACTION_SQUADMEMBER_GETBYID_ERROR, ACTION_SQUADMEMBER_GETBYID_SUCCESS } from "../actions/squadMemberActions";


// Get one
interface GetOneState {
    getOneAttempting: boolean,
    getOneError: string,
    getOneSuccess: boolean,
    squadMember: null
}

const initialGetOneState : GetOneState = {
    getOneAttempting: false,
    getOneError: "",
    getOneSuccess: false,
    squadMember: null
}
export const getSquadMemberReducer = ( state = initialGetOneState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_SQUAD_GETBYPLAYERID_ATTEMPTING:
            return {
                ...state,
                getOneAttempting: true,
                getOneError: "",
                getOneSuccess: false
            }
        case ACTION_SQUAD_GETBYPLAYERID_SUCCESS:
            return {
                ...state,
                getOneAttempting: false,
                getOneError: "",
                getOneSuccess: true,
                squadMember: action.payload
            }
        case ACTION_SQUAD_GETBYPLAYERID_ERROR:
            return {
                ...state,
                getOneAttempting: false,
                getOneError: action.payload,
                getOneSuccess: false
            }
        case ACTION_SQUAD_GETBYPLAYERID_COMPLETE:
            return {
                ...initialGetOneState
            }
        default:
            return state
    }
}

// Create
interface CreateSquadMemberState {
    createSquadMemberAttempting: boolean,
    createSquadMemberError: string,
    createSquadMemberSuccess: boolean
}

const initialCreateSquadMemberState : CreateSquadMemberState = {
    createSquadMemberAttempting: false,
    createSquadMemberError: "",
    createSquadMemberSuccess: false
}
export const createSquadMemberReducer = ( state = initialCreateSquadMemberState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_SQUADMEMBER_CREATE_ATTEMPTING:
            return {
                ...state,
                createSquadMemberAttempting: true,
                createSquadMemberError: "",
                createSquadMemberSuccess: false
            }
        case ACTION_SQUADMEMBER_CREATE_SUCCESS:
            return {
                ...state,
                createSquadMemberAttempting: false,
                createSquadMemberError: "",
                createSquadMemberSuccess: true
            }
        case ACTION_SQUADMEMBER_CREATE_ERROR:
            return {
                ...state,
                createSquadMemberAttempting: false,
                createSquadMemberError: action.payload,
                createSquadMemberSuccess: false
            }
        case ACTION_SQUADMEMBER_CREATE_COMPLETE:
            return {
                ...initialCreateSquadMemberState
            }
        default:
            return state;
    }
}

// Update
interface UpdateSquadMemberState {
    updateSquadMemberAttempting: boolean,
    updateSquadMemberError: string,
    updateSquadMemberSuccess: boolean
}

const initialUpdateSquadMemberState : UpdateSquadMemberState = {
    updateSquadMemberAttempting: false,
    updateSquadMemberError: "",
    updateSquadMemberSuccess: false
}
export const updateSquadMemberReducer = ( state = initialUpdateSquadMemberState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_SQUADMEMBER_CREATE_ATTEMPTING:
            return {
                ...state,
                updateSquadMemberAttempting: true,
                updateSquadMemberError: "",
                updateSquadMemberSuccess: false
            }
        case ACTION_SQUADMEMBER_CREATE_SUCCESS:
            return {
                ...state,
                updateSquadMemberAttempting: false,
                updateSquadMemberError: "",
                updateSquadMemberSuccess: true
            }
        case ACTION_SQUADMEMBER_CREATE_ERROR:
            return {
                ...state,
                updateSquadMemberAttempting: false,
                updateSquadMemberError: action.payload,
                updateSquadMemberSuccess: false
            }
        case ACTION_SQUADMEMBER_CREATE_COMPLETE:
            return {
                ...initialUpdateSquadMemberState
            }
        default:
            return state;
    }
}

// delete by id
interface IDeleteSquadMemberState {
    deleteSquadMemberAttempting: boolean,
    deleteSquadMemberError: string,
    deleteSquadMemberSuccess: boolean
}

const initialDeleteSquadMemberState : IDeleteSquadMemberState = {
    deleteSquadMemberAttempting: false,
    deleteSquadMemberError: "",
    deleteSquadMemberSuccess: false
}

export const deleteSquadMemberReducer = ( state = initialDeleteSquadMemberState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_SQUADMEMBER_DELETE_ATTEMPTING:
            return {
                ...state,
                deleteSquadMemberAttempting: true,
                deleteSquadMemberError: "",
                deleteSquadMemberSuccess: false
            }
        case ACTION_SQUADMEMBER_DELETE_SUCCESS:
            return {
                ...state,
                deleteSquadMemberAttempting: false,
                deleteSquadMemberError: "",
                deleteSquadMemberSuccess: true

            }
        case ACTION_SQUADMEMBER_DELETE_ERROR:
            return {
                ...state,
                deleteSquadMemberAttempting: false,
                deleteSquadMemberError: action.payload,
                deleteSquadMemberSuccess: false
            }
        case ACTION_SQUADMEMBER_DELETE_COMPLETE:
            return {
                ...initialDeleteSquadMemberState
            }
        default:
            return state;
    }
}