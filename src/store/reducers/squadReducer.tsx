import { AnyAction } from "redux";
import { start } from "repl";
import { ISquadMember } from "../../types/SquadMemberTypes";
import { ISquad } from "../../types/SquadTypes";
import { ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ATTEMPTING, ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_COMPLETE, ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ERROR, ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_SUCCESS, ACTION_SQUADMEMBER_GETBYSQUADID_ATTEMPTING, ACTION_SQUADMEMBER_GETBYSQUADID_COMPLETE, ACTION_SQUADMEMBER_GETBYSQUADID_ERROR, ACTION_SQUADMEMBER_GETBYSQUADID_SUCCESS, ACTION_SQUADS_GETBYGAMEID_ATTEMPTING, ACTION_SQUADS_GETBYGAMEID_COMPLETE, ACTION_SQUADS_GETBYGAMEID_ERROR, ACTION_SQUADS_GETBYGAMEID_SUCCESS, ACTION_SQUAD_CREATE_ATTEMPTING, ACTION_SQUAD_CREATE_COMPLETE, ACTION_SQUAD_CREATE_ERROR, ACTION_SQUAD_CREATE_SUCCESS, ACTION_SQUAD_GETALL_ATTEMPTING, ACTION_SQUAD_GETALL_COMPLETE, ACTION_SQUAD_GETALL_ERROR, ACTION_SQUAD_GETALL_SUCCESS, ACTION_SQUAD_GETBYID_ATTEMPTING, ACTION_SQUAD_GETBYID_COMPLETE, ACTION_SQUAD_GETBYID_ERROR, ACTION_SQUAD_GETBYID_SUCCESS, ACTION_SQUAD_GETBYPLAYERID_ATTEMPTING, ACTION_SQUAD_GETBYPLAYERID_COMPLETE, ACTION_SQUAD_GETBYPLAYERID_ERROR, ACTION_SQUAD_GETBYPLAYERID_SUCCESS } from "../actions/squadActions";
import { ACTION_SQUADMEMBER_CREATE_ATTEMPTING } from "../actions/squadMemberActions";


// Get all
interface GetAllState {
    getAllAttempting: boolean,
    getAllError: string,
    getAllSuccess: boolean,
    allSquads: ISquad[]
}

const initialGetAllState: GetAllState = {
    getAllAttempting: false,
    getAllError: "",
    getAllSuccess: false,
    allSquads: []
}

export const getAllSquadsReducer = (state = initialGetAllState, action: AnyAction) => {
    switch (action.type) {
        case ACTION_SQUAD_GETALL_ATTEMPTING:
            return {
                ...state,
                getAllAttempting: true,
                getAllError: "",
                getALLSuccess: false
            }
        case ACTION_SQUAD_GETALL_COMPLETE:
            return {
                ...initialGetAllState
            }
        case ACTION_SQUAD_GETALL_ERROR:
            return {
                ...state,
                getAllAttempting: false,
                getAllError: action.payload,
                getAllSuccess: false
            }
        case ACTION_SQUAD_GETALL_SUCCESS:
            return {
                ...state,
                getAllAttempting: false,
                getAllError: "",
                getAllSuccess: true,
                allSquads: [...action.payload]
            }
        default:
            return state;
    }
}

// Get one
interface GetOneState {
    getOneAttempting: boolean,
    getOneError: string,
    getOneSuccess: boolean,
    squad: ISquad | null
}

const initialGetOneState: GetOneState = {
    getOneAttempting: false,
    getOneError: "",
    getOneSuccess: false,
    squad: null
}

export const getSquadReducer = (state = initialGetOneState, action: AnyAction) => {
    switch (action.type) {
        case ACTION_SQUAD_GETBYID_ATTEMPTING:
            return {
                ...state,
                getOneAttempting: true,
                getOneError: "",
                getOneSuccess: false
            }
        case ACTION_SQUAD_GETBYID_SUCCESS:
            return {
                ...state,
                getOneAttempting: false,
                getOneError: "",
                getOneSuccess: true,
                squad: action.payload
            }
        case ACTION_SQUAD_GETBYID_ERROR:
            return {
                ...state,
                getOneAttempting: false,
                getOneError: action.payload,
                getOneSuccess: false
            }
        case ACTION_SQUAD_GETBYID_COMPLETE:
            return {
                ...initialGetOneState
            }
        default:
            return state
    }
}

// Create 
interface CreateSquadState {
    createSquadAttempting: boolean,
    createSquadError: string,
    createSquadSuccess: boolean
}

const initialCreateSquadState : CreateSquadState = {
    createSquadAttempting: false,
    createSquadError: "",
    createSquadSuccess: false
}

export const createSquadReducer = ( state = initialCreateSquadState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_SQUAD_CREATE_ATTEMPTING:
            return {
                ...state,
                createSquadAttempting: true,
                createSquadError: "",
                createSquadSuccess: false
            }
        case ACTION_SQUAD_CREATE_SUCCESS:
            return {
                ...state,
                createSquadAttempting: false,
                createSquadError: "",
                createSquadSuccess: true
            }
        case ACTION_SQUAD_CREATE_ERROR:
            return {
                ...state,
                createSquadAttempting: false,
                createSquadError: action.payload,
                createSquadSuccess: false
            }
        case ACTION_SQUAD_CREATE_COMPLETE:
            return {
                ...initialCreateSquadState
            }
        default:
            return state;
    }
}

// Update
interface UpdateSquadState {
    updateSquadAttempting: boolean,
    updateSquadError: string,
    updateSquadSuccess: boolean
}

const initialUpdateSquadState : UpdateSquadState = {
    updateSquadAttempting: false,
    updateSquadError: "",
    updateSquadSuccess: false
}

export const updateSquadReducer = ( state = initialUpdateSquadState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_SQUAD_CREATE_ATTEMPTING:
            return {
                ...state,
                updateSquadAttempting: true,
                updateSquadError: "",
                updateSquadSuccess: false
            }
        case ACTION_SQUAD_CREATE_SUCCESS:
            return {
                ...state,
                updateSquadAttempting: false,
                updateSquadError: "",
                updateSquadSuccess: true
            }
        case ACTION_SQUAD_CREATE_ERROR:
            return {
                ...state,
                updateSquadAttempting: false,
                updateSquadError: action.payload,
                updateSquadSuccess: false
            }
        case ACTION_SQUAD_CREATE_COMPLETE:
            return {
                ...initialUpdateSquadState
            }
        default:
            return state;
    }
}

// Get all members in one game
interface IGetAllMembersState {
    getAllMembersInSquadAttempting: boolean,
    getAllMembersInSquadError: string,
    getAllMembersInSquadSuccess: boolean,
    members: ISquadMember[]
}

const initialGetAllMembersState: IGetAllMembersState = {
    getAllMembersInSquadAttempting: false,
    getAllMembersInSquadError: "",
    getAllMembersInSquadSuccess: false,
    members: []
}

export const getAllMembersInSquadReducer = (state = initialGetAllMembersState, action: AnyAction) => {
    switch (action.type) {
        case ACTION_SQUADMEMBER_GETBYSQUADID_ATTEMPTING:
            return {
                ...state,
                getAllMembersInSquadAttempting: true,
                getAllMembersInSquadError: "",
                getAllMembersInSquadSuccess: false,
                members: []
            }
        case ACTION_SQUADMEMBER_GETBYSQUADID_SUCCESS:
            return {
                ...state,
                getAllMembersInSquadAttempting: false,
                getAllMembersInSquadError: "",
                getAllMembersInSquadSuccess: true,
                members: [...action.payload]
            }
        case ACTION_SQUADMEMBER_GETBYSQUADID_ERROR:
            return {
                ...state,
                getAllMembersInSquadAttempting: false,
                getAllMembersInSquadError: action.payload,
                getAllMembersInSquadSuccess: false,
                members: []
            }
        case ACTION_SQUADMEMBER_GETBYSQUADID_COMPLETE:
            return {
                ...initialGetAllMembersState
            }
        default:
            return state
    }
}

// Get all squads in one game
interface GetAllSquadsState {
    getAllSquadssAttempting: boolean,
    getAllSquadssError: string,
    getAllSquadssSuccess: boolean,
    squads: ISquad[]
}

const initialGetAllSquadsState: GetAllSquadsState = {
    getAllSquadssAttempting: false,
    getAllSquadssError: "",
    getAllSquadssSuccess: false,
    squads: []
}

export const getAllSquadsInGameReducer = (state = initialGetAllSquadsState, action: AnyAction) => {
    switch (action.type) {
        case ACTION_SQUADS_GETBYGAMEID_ATTEMPTING:
            return {
                ...state,
                getAllSquadssAttempting: true,
                getAllSquadssError: "",
                getAllSquadssSuccess: false
            }
        case ACTION_SQUADS_GETBYGAMEID_SUCCESS:
            return {
                ...state,
                getAllSquadssAttempting: false,
                getAllSquadssError: "",
                getAllSquadssSuccess: true,
                squads: [...action.payload]
            }
        case ACTION_SQUADS_GETBYGAMEID_ERROR:
            return {
                ...state,
                getAllSquadssAttempting: false,
                getAllSquadssError: action.payload,
                getAllSquadssSuccess: false
            }
        case ACTION_SQUADS_GETBYGAMEID_COMPLETE:
            return {
                ...initialGetAllSquadsState
            }
        default:
            return state
    }
}

// Get all killed squadmembers in one squad
interface GetKilledSquadmemberState {
    getKilledSquadmemberAttempting: boolean,
    getKilledSquadmemberError: string,
    getKilledSquadmemberSuccess: boolean,
    //killedmembers: number[]
}

const initialGetKilledSquadmemberState: GetKilledSquadmemberState = {
    getKilledSquadmemberAttempting: false,
    getKilledSquadmemberError: "",
    getKilledSquadmemberSuccess: false,
    //killedmembers: []
}
export const getKilledMembersReducer = (state = initialGetKilledSquadmemberState, action: AnyAction) => {
    switch (action.type) {
        case ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ATTEMPTING:
            return {
                ...state,
                getKilledSquadmemberAttempting: true,
                getKilledSquadmemberError: "",
                getKilledSquadmemberSuccess: false
            }
        case ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_SUCCESS:
            return {
                ...state,
                getKilledSquadmemberAttempting: false,
                getKilledSquadmemberError: "",
                getKilledSquadmemberSuccess: true,
                //killedmembers: [...action.payload]
            }
        case ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ERROR:
            return {
                ...state,
                getKilledSquadmemberAttempting: false,
                getKilledSquadmemberError: action.payload,
                getKilledSquadmemberSuccess: false
            }
        case ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_COMPLETE:
            return {
                ...initialGetKilledSquadmemberState
            }
        default:
            return state
    }
}

// Get one
interface GetOneSquadByPlayerState {
    getOneSquadByPlayerAttempting: boolean,
    getOneSquadByPlayerError: string,
    getOneSquadByPlayerSuccess: boolean,
    squad: ISquad | null
}

const initialGetOneSquadByPlayerState: GetOneSquadByPlayerState = {
    getOneSquadByPlayerAttempting: false,
    getOneSquadByPlayerError: "",
    getOneSquadByPlayerSuccess: false,
    squad: null
}

export const getSquadByPlayerReducer = (state = initialGetOneSquadByPlayerState, action: AnyAction) => {
    switch (action.type) {
        case ACTION_SQUAD_GETBYPLAYERID_ATTEMPTING:
            return {
                ...state,
                getOneSquadByPlayerAttempting: true,
                getOneSquadByPlayerError: "",
                getOneSquadByPlayerSuccess: false,
                squad: null
            }
        case ACTION_SQUAD_GETBYPLAYERID_SUCCESS:
            return {
                ...state,
                getOneSquadByPlayerAttempting: false,
                getOneSquadByPlayerError: "",
                getOneSquadByPlayerSuccess: true,
                squad: action.payload
            }
        case ACTION_SQUAD_GETBYPLAYERID_ERROR:
            return {
                ...state,
                getOneSquadByPlayerAttempting: false,
                getOneSquadByPlayerError: action.payload,
                getOneSquadByPlayerSuccess: false,
                squad: null
            }
        case ACTION_SQUAD_GETBYPLAYERID_COMPLETE:
            return {
                ...initialGetOneState
            }
        default:
            return state
    }
}