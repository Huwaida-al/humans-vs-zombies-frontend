import { AnyAction } from "redux"
import { ACTION_KILL_REGISTER_ATTEMPTING, ACTION_KILL_REGISTER_COMPLETE, ACTION_KILL_REGISTER_ERROR, ACTION_KILL_REGISTER_SUCCESS } from "../actions/killActions"

interface RegisterKillState {
    registerKillAttempting: boolean,
    registerKillError: string,
    registerKillSuccess: boolean
}

const initialRegisterKillState : RegisterKillState = {
    registerKillAttempting: false,
    registerKillError: "",
    registerKillSuccess: false
}

export const registerKillReducer = ( state = initialRegisterKillState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_KILL_REGISTER_ATTEMPTING:
            return {
                ...state,
                registerKillAttempting: true,
                registerKillError: "",
                registerKillSuccess: false
            }
        case ACTION_KILL_REGISTER_SUCCESS:
            return {
                ...state,
                registerKillAttempting: false,
                registerKillError: "",
                registerKillSuccess: true
            }
        case ACTION_KILL_REGISTER_ERROR:
            return {
                ...state,
                registerKillAttempting: false,
                registerKillError: action.payload,
                registerKillSuccess: false
            }
        case ACTION_KILL_REGISTER_COMPLETE:
            return {
                ...initialRegisterKillState
            }
        default:
            return state

        }
    }