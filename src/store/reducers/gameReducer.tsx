import { AnyAction } from "redux";
import { IGame } from "../../types/GameTypes";
import { ACTION_GAMES_FETCH_ATTEMPTING, ACTION_GAMES_FETCH_COMPLETE, ACTION_GAMES_FETCH_ERROR, 
    ACTION_GAMES_FETCH_SUCCESS, ACTION_GAME_CREATE_ATTEMPTING, ACTION_GAME_CREATE_COMPLETE, ACTION_GAME_CREATE_ERROR, ACTION_GAME_CREATE_SUCCESS, ACTION_GAME_FETCH_ATTEMPTING,
    ACTION_GAME_FETCH_ERROR, ACTION_GAME_FETCH_SUCCESS, ACTION_GAME_UPDATE_ATTEMPTING, ACTION_GAME_UPDATE_COMPLETE, ACTION_GAME_UPDATE_ERROR, ACTION_GAME_UPDATE_SUCCESS,
    ACTION_GAME_FETCH_COMPLETE } from "../actions/gameActions"


// Get all
interface GetAllState {
    getAllAttempting: boolean,
    getAllError: string,
    getAllSuccess: boolean,
    games: IGame[]
}

const initialGetAllState : GetAllState = {
    getAllAttempting: false,
    getAllError: "",
    getAllSuccess: false,
    games: []
}

export const getAllGamesReducer = ( state = initialGetAllState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_GAMES_FETCH_ATTEMPTING:
            return {
                ...state,
                getAllAttempting: true,
                getAllError: "",
                getAllSuccess: false,
                games: []
            }
        case ACTION_GAMES_FETCH_COMPLETE:
            return {
                ...initialGetAllState
            }
        case ACTION_GAMES_FETCH_ERROR:
            return {
                ...state,
                getAllAttempting: false,
                getAllError: action.payload,
                getAllSuccess: false,
                games: []
            }
        case ACTION_GAMES_FETCH_SUCCESS:
            return {
                ...state,
                getAllAttempting: false,
                getAllError: "",
                getAllSuccess: true,
                games: [...action.payload]
            }
        default: 
            return state;
    }
}

// Get one
interface GetOneState {
    getOneAttempting: boolean,
    getOneError: string,
    getOneSuccess: boolean
    game : IGame | null
}

const initialGetOneState : GetOneState = {
    getOneAttempting: false,
    getOneError: "",
    getOneSuccess: false,
    game : null
}

export const getGameReducer = ( state = initialGetOneState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_GAME_FETCH_ATTEMPTING:
            return {
                ...state,
                getOneAttempting: true,
                getOneError: "",
                getOneSuccess: false,
                game : null
            }
        case ACTION_GAME_FETCH_SUCCESS:
            return {
                ...state,
                getOneAttempting: false,
                getOneError: "",
                getOneSuccess: true,
                game: action.payload
            }
        case ACTION_GAME_FETCH_ERROR:
            return {
                ...state,
                getOneAttempting: false,
                getOneError: action.payload,
                getOneSuccess: false,
                game : null
                
            }
        case ACTION_GAME_FETCH_COMPLETE:
            return {
                ...initialGetAllState
            }
        default:
            return state
    }
}

// Create
interface CreateGameState {
    createGameAttempting: boolean,
    createGameError: string,
    createGameSuccess: boolean
}

const initialCreateGameState : CreateGameState = {
    createGameAttempting: false,
    createGameError: "",
    createGameSuccess: false
}

export const createGameReducer = ( state = initialCreateGameState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_GAME_CREATE_ATTEMPTING:
            return {
                ...state,
                createGameAttempting: true,
                createGameError: "",
                createGameSuccess: false
            }
        case ACTION_GAME_CREATE_SUCCESS:
            return {
                ...state,
                createGameAttempting: false,
                createGameError: "",
                createGameSuccess: true
            }
        case ACTION_GAME_CREATE_ERROR:
            return {
                ...state,
                createGameAttempting: false,
                createGameError: action.payload,
                createGameSuccess: false
            }
        case ACTION_GAME_CREATE_COMPLETE:
            return {
                ...initialCreateGameState
            }
        default:
            return state;
    }
}

// Update
interface UpdateGameState {
    updateGameAttempting: boolean,
    updateGameError: string,
    updateGameSuccess: boolean
}

const initialUpdateGameState : UpdateGameState = {
    updateGameAttempting: false,
    updateGameError: "",
    updateGameSuccess: false
}

export const updateGameReducer = ( state = initialUpdateGameState, action : AnyAction) => {
    switch (action.type) {
        case ACTION_GAME_UPDATE_ATTEMPTING:
            return {
                ...state,
                updateGameAttempting: true,
                updateGameError: "",
                updateGameSuccess: false
            }
        case ACTION_GAME_UPDATE_SUCCESS:
            return {
                ...state,
                updateGameAttempting: false,
                updateGameError: "",
                updateGameSuccess: true
            }
        case ACTION_GAME_UPDATE_ERROR:
            return {
                ...state,
                updateGameAttempting: false,
                updateGameError: action.payload,
                updateGameSuccess: false
            }
        case ACTION_GAME_UPDATE_COMPLETE:
            return {
                ...initialUpdateGameState
            }
        default:
            return state;
    }
}