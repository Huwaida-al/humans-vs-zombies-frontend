import { combineReducers } from 'redux';
import { sessionReducer, playerSessionReducer, allKilledSessionReducer, allSquadMembersSessionReducer } from './sessionReducer';
import { loginReducer, registerUserReducer } from './userReducer';
import { getAllGamesReducer, getGameReducer, createGameReducer, updateGameReducer } from './gameReducer';
import { createPlayerReducer, deletePlayerReducer, getPlayerInGameReducer, getPlayerReducer, updatePlayerReducer } from './playerReducer';
import { registerKillReducer } from './killReducer';
import { getAllMissionsReducer, createMissionReducer, getAllMissionsByGameIdReducer, deleteMissionReducer} from './missionReducer';
import { ACTION_SESSION_CLEAR } from '../actions/sessionActions';
import { createSquadReducer, getAllMembersInSquadReducer, getAllSquadsInGameReducer, getSquadByPlayerReducer, getAllSquadsReducer, getKilledMembersReducer, getSquadReducer, updateSquadReducer } from './squadReducer';
import { createSquadMemberReducer, getSquadMemberReducer, updateSquadMemberReducer, deleteSquadMemberReducer } from './squadMemberReducer';
//import { createPlayerReducer, getAllPlayersReducer, getPlayerReducer, updatePlayerReducer } from './playerReducer';

// Bundle all login files for store
const appReducer = combineReducers({
    // user
    login: loginReducer,
    session: sessionReducer,
    register: registerUserReducer,
    // game
    allGames: getAllGamesReducer,
    game: getGameReducer,
    update: updateGameReducer,
    createGameReducer,
    // player
    getPlayer: getPlayerReducer,
    createPlayer: createPlayerReducer,
    updatePlayer: updatePlayerReducer,
    deletePlayer: deletePlayerReducer,
    createPlayerReducer,
    playerSessionReducer,
    getPlayersInGame: getPlayerInGameReducer,
    // kill/bite
    registerKillReducer,
    // missions
    createMissionReducer,
    allMissions : getAllMissionsReducer,
    allMissionsByGameId: getAllMissionsByGameIdReducer,
    deleteMission : deleteMissionReducer,
    //allMissions : getAllMissionsReducer,
    
    // squad
    // allSquads : getAllSquadsReducer,
    // squad : getSquadReducer,
    allKilledSessionReducer,
    getSquadByPlayerReducer,
    createsquad: createSquadReducer,
    getAllMembersInSquad: getAllMembersInSquadReducer,
    allSquadsInGame: getAllSquadsInGameReducer,
    // updateSquadReducer,

    // squadmembers
    killed: getKilledMembersReducer,
    membersSession: allSquadMembersSessionReducer,
    // squadMembers : getSquadMemberReducer,
    createSquadMemberReducer,
    deleteSquadMemberReducer,
    updateSquadMemberReducer,
})

const rootReducer = (state: any, action: any) => {
    if (action.type === ACTION_SESSION_CLEAR) {
        state = undefined
    }
    return appReducer(state, action)
}


export default appReducer