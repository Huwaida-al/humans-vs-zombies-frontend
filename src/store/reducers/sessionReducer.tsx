import { ISessionPlayer } from "../../types/PlayerTypes";
import { ISquadMember } from "../../types/SquadMemberTypes";
import { ACTION_SESSION_SET, ACTION_SESSION_CLEAR, ACTION_SESSION_LOGOUT, ACTION_PLAYER_SESSION_SET, 
    ACTION_PLAYER_SESSION_CLEAR, ACTION_PLAYER_SESSION_BITE, ACTION_KILLEDSQUADMEMBER_SESSION_SET, 
    ACTION_KILLEDSQUADMEMBER_SESSION_CLEAR, ACTION_SQUADMEMBER_SESSION_SET, ACTION_SQUADMEMBER_SESSION_CLEAR } 
    from "../actions/sessionActions"

const initialState = {
    username: '',
    admin: false,
    lastLogin: '',
    createdAt: '',
    token: '',
    loggedIn: false,
    players: []
}

export const sessionReducer = (state = { ...initialState }, action: { type: any; payload: any; }) => {
    switch (action.type) {

        case ACTION_SESSION_SET:
            return {
                ...state,
                loggedIn: true,
                username: action.payload.id,
                admin: action.payload.isAdmin,
                players: action.payload.players

            }
        case ACTION_SESSION_CLEAR:
            return {
                ...initialState
            }
        case ACTION_SESSION_LOGOUT:
            return {
                ...initialState
            }
        default:
            return state
    }
}

const initialPlayerSessionState : ISessionPlayer = {
    playerId: undefined,
    biteCode: undefined,
    gameId: undefined,
    isHuman: undefined,
    isPatientZero: undefined,
    userId: undefined,
    squadId: undefined
}

export const playerSessionReducer = (state = { ...initialPlayerSessionState }, action: { type: any; payload: any; }) => {
    switch (action.type) {

        case ACTION_PLAYER_SESSION_SET:
            return {
                ...state,
                playerId: action.payload.id,
                biteCode: action.payload.biteCode,
                gameId: action.payload.gameId,
                isHuman: action.payload.isHuman,
                isPatientZero: action.payload.isPatientZero,
                userId: action.payload.userId,
                squadId: action.payload.squadId
            }
        case ACTION_PLAYER_SESSION_CLEAR:
            return {
                ...initialPlayerSessionState
            }
        default:
            return state
    }
}

// interface IAllPlayersSessionState {
//     inGamePlayers : ISessionPlayer[]
// }

// const initialAllPlayerSessionState : IAllPlayersSessionState = {
//     inGamePlayers : []
// }


// export const allPlayerSessionReducer = (state = { ...initialAllPlayerSessionState }, action: { type: any; payload: any; }) => {
//     switch (action.type) {

//         case ACTION_ALLPLAYER_SESSION_SET:
//             return {
//                 ...state,
//                 inGamePlayers: action.payload
//             }
//         case ACTION_ALLPLAYER_SESSION_CLEAR:
//             return {
//                 ...initialAllPlayerSessionState
//             }
//         default:
//             return state
//     }
// }


interface KilledSessionState {
    Killed : number[]
}

const initialKilledSessionState : KilledSessionState = {
    Killed : []
}


export const allKilledSessionReducer = (state = { ...initialKilledSessionState }, action: { type: any; payload: any; }) => {
    switch (action.type) {

        case ACTION_KILLEDSQUADMEMBER_SESSION_SET:
            return {
                ...state,
                Killed: [...action.payload]
            }
        case ACTION_KILLEDSQUADMEMBER_SESSION_CLEAR:
            return {
                ...initialKilledSessionState
            }
        default:
            return state
    }
}

interface ISquadMemberSessionState {
    SquadMembers : ISquadMember[]
}

const SquadMemberSessionState : ISquadMemberSessionState = {
    SquadMembers : []
}


export const allSquadMembersSessionReducer = (state = { ...SquadMemberSessionState }, action: { type: any; payload: any; }) => {
    switch (action.type) {

        case ACTION_SQUADMEMBER_SESSION_SET:
            return {
                ...state,
                SquadMembers: [...action.payload]
            }
        case ACTION_SQUADMEMBER_SESSION_CLEAR:
            return {
                ...SquadMemberSessionState
            }
        default:
            return state
    }
}