import { ISquadMember } from "../../types/SquadMemberTypes"
import { ISquad, ICreateSquad } from "../../types/SquadTypes"

//Get all Squads
export const ACTION_SQUAD_GETALL_ATTEMPTING = '[getAll] Squad ATTEMPT'
export const ACTION_SQUAD_GETALL_SUCCESS = '[getAll] Squad SUCCESS'
export const ACTION_SQUAD_GETALL_ERROR = '[getAll] Squad ERROR'
export const ACTION_SQUAD_GETALL_COMPLETE = '[getAll] Squad COMPLETE'
export const squadsFetchAttemptingAction = () => ({
    type: ACTION_SQUAD_GETALL_ATTEMPTING
})
export const squadsFetchSuccessAction = (squads: ISquad[]) => ({
    type: ACTION_SQUAD_GETALL_SUCCESS,
    payload: squads
})
export const squadsFetchErrorAction = (message: string) => ({
    type: ACTION_SQUAD_GETALL_ERROR,
    payload: message
})
export const squadsFetchCompleteAction = () => ({
    type: ACTION_SQUAD_GETALL_COMPLETE
})

//Get Squad by Id
export const ACTION_SQUAD_GETBYID_ATTEMPTING = '[getAllById] Squad ATTEMPT'
export const ACTION_SQUAD_GETBYID_SUCCESS = '[getAllById] Squad SUCCESS'
export const ACTION_SQUAD_GETBYID_ERROR = '[getAllById] Squad ERROR'
export const ACTION_SQUAD_GETBYID_COMPLETE = '[getAllById] Squad COMPLETE'
export const squadFetchByIdAttemptingAction = (squadId: any) => ({
    type: ACTION_SQUAD_GETBYID_ATTEMPTING,
    payload: squadId
})
export const squadFetchByIdSuccessAction = (squad: ISquad) => ({
    type: ACTION_SQUAD_GETBYID_SUCCESS,
    payload: squad
})
export const squadFetchByIdErrorAction = (message: string) => ({
    type: ACTION_SQUAD_GETBYID_ERROR,
    payload: message
})
export const squadFetchByIdCompleteAction = () => ({
    type: ACTION_SQUAD_GETBYID_COMPLETE
})

// Create Squad
export const ACTION_SQUAD_CREATE_ATTEMPTING = '[create] squad ATTEMPT'
export const ACTION_SQUAD_CREATE_SUCCESS    = '[create] squad SUCCESS'
export const ACTION_SQUAD_CREATE_ERROR      = '[create] squad ERROR'
export const ACTION_SQUAD_CREATE_COMPLETE   = '[create] squad COMPLETE'

export const squadsCreateAttemptingAction = (squad: ICreateSquad, playerID : any) => ({
    type: ACTION_SQUAD_CREATE_ATTEMPTING,
    payload: squad,
    playerID: playerID
})
export const squadsCreateSuccessAction = (squads: ISquad, playerID : any) => ({
    type: ACTION_SQUAD_CREATE_SUCCESS,
    payload: squads,
    playerID: playerID
})
export const squadsCreateErrorAction = (message: string) => ({
    type: ACTION_SQUAD_CREATE_ERROR,
    payload: message
})
export const squadsCreateCompleteAction = () => ({
    type: ACTION_SQUAD_CREATE_COMPLETE
})


//Get all members in Squad by Id 
export const ACTION_SQUADMEMBER_GETBYSQUADID_ATTEMPTING = '[getAllMembersById] Squad ATTEMPT'
export const ACTION_SQUADMEMBER_GETBYSQUADID_SUCCESS = '[getAllMembersById] Squad SUCCESS'
export const ACTION_SQUADMEMBER_GETBYSQUADID_ERROR = '[getAllMembersById] Squad ERROR'
export const ACTION_SQUADMEMBER_GETBYSQUADID_COMPLETE = '[getAllMembersById] Squad COMPLETE'

export const squadMemberInSquadFetchByIdAttemptingAction = (squadId: any) => ({
    type: ACTION_SQUADMEMBER_GETBYSQUADID_ATTEMPTING,
    payload: squadId
})
export const squadMemberInSquadFetchByIdSuccessAction = (squadmembers: ISquadMember[]) => ({
    type: ACTION_SQUADMEMBER_GETBYSQUADID_SUCCESS,
    payload: squadmembers
})
export const squadMemberInSquadFetchByIdErrorAction = (message: string) => ({
    type: ACTION_SQUADMEMBER_GETBYSQUADID_ERROR,
    payload: message
})
export const squadMemberInSquadFetchByIdCompleteAction = () => ({
    type: ACTION_SQUADMEMBER_GETBYSQUADID_COMPLETE
})


//Get all squads in Game by Id
export const ACTION_SQUADS_GETBYGAMEID_ATTEMPTING = '[getAllSquadsById] Squad ATTEMPT'
export const ACTION_SQUADS_GETBYGAMEID_SUCCESS = '[getAllSquadsById] Squad SUCCESS'
export const ACTION_SQUADS_GETBYGAMEID_ERROR = '[getAllSquadsById] Squad ERROR'
export const ACTION_SQUADS_GETBYGAMEID_COMPLETE = '[getAllSquadsById] Squad COMPLETE'

export const squadsInGameFetchByIdAttemptingAction = (gameId: any) => ({
    type: ACTION_SQUADS_GETBYGAMEID_ATTEMPTING,
    payload: gameId
})
export const squadsInGameFetchByIdSuccessAction = (squads: ISquad[]) => ({
    type: ACTION_SQUADS_GETBYGAMEID_SUCCESS,
    payload: squads
})
export const squadsInGameFetchByIdErrorAction = (message: string) => ({
    type: ACTION_SQUADS_GETBYGAMEID_ERROR,
    payload: message
})
export const squadsInGameFetchByIdCompleteAction = () => ({
    type: ACTION_SQUADS_GETBYGAMEID_COMPLETE
})

//Get all killed members in squad
export const ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ATTEMPTING = '[getKilledMembers] Squad ATTEMPT'
export const ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_SUCCESS = '[getKilledMembers] Squad SUCCESS'
export const ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ERROR = '[getKilledMembers] Squad ERROR'
export const ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_COMPLETE = '[getKilledMembers] Squad COMPLETE'

export const killedMembersFetchByIdAttemptingAction = (squadId: any) => ({
    type: ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ATTEMPTING,
    payload: squadId
})
export const killedMembersFetchByIdSuccessAction = (killedmembers: number[]) => ({
    type: ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_SUCCESS,
    payload: killedmembers
})
export const killedMembersFetchByIdErrorAction = (message: string) => ({
    type: ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_ERROR,
    payload: message
})
export const killedMembersFetchByIdCompleteAction = () => ({
    type: ACTION_KILLEDSQUADMEMBER_GETBYSQUADID_COMPLETE
})

//Get Squad by Id
export const ACTION_SQUAD_GETBYPLAYERID_ATTEMPTING = '[getSquadByPlayerId] Squad ATTEMPT'
export const ACTION_SQUAD_GETBYPLAYERID_SUCCESS = '[getSquadByPlayerId] Squad SUCCESS'
export const ACTION_SQUAD_GETBYPLAYERID_ERROR = '[getSquadByPlayerId] Squad ERROR'
export const ACTION_SQUAD_GETBYPLAYERID_COMPLETE = '[getSquadByPlayerId] Squad COMPLETE'

export const squadFetchByPlayerIdAttemptingAction = (playerId: any) => ({
    type: ACTION_SQUAD_GETBYPLAYERID_ATTEMPTING,
    payload: playerId
})
export const squadFetchByPlayerIdSuccessAction = (squad: ISquad) => ({
    type: ACTION_SQUAD_GETBYPLAYERID_SUCCESS,
    payload: squad
})
export const squadFetchByPlayerIdErrorAction = (message: string) => ({
    type: ACTION_SQUAD_GETBYPLAYERID_ERROR,
    payload: message
})
export const squadFetchByPlayerIdCompleteAction = () => ({
    type: ACTION_SQUAD_GETBYPLAYERID_COMPLETE
})