import { IEditPlayer, IPlayer, RegisterPlayerCredentials } from "../../types/PlayerTypes";

// fetch all players
export const ACTION_PLAYERS_FETCH_ATTEMPTING = '[getAllPlayers] ATTEMPT'
export const ACTION_PLAYERS_FETCH_SUCCESS    = '[getAllPlayers] SUCCESS'
export const ACTION_PLAYERS_FETCH_ERROR      = '[getAllPlayers] ERROR'
export const ACTION_PLAYERS_FETCH_COMPLETE   = '[getAllPlayers] COMPLETE'

export const playersFetchAttemptAction = () => ({
    type: ACTION_PLAYERS_FETCH_ATTEMPTING
})

export const playersFetchSuccessAction = (players : IPlayer[]) => ({
    type: ACTION_PLAYERS_FETCH_SUCCESS,
    payload: players
})

export const playersFetchErrorAction = (message : string) => ({
    type: ACTION_PLAYERS_FETCH_ERROR,
    payload: message
})

export const playersFetchCompleteAction = () => ({
    type: ACTION_PLAYERS_FETCH_COMPLETE
})

// create player
export const ACTION_PLAYER_CREATE_ATTEMPTING = '[create player] ATTEMPT'
export const ACTION_PLAYER_CREATE_SUCCESS    = '[create player] SUCCESS'
export const ACTION_PLAYER_CREATE_ERROR      = '[create player] ERROR'
export const ACTION_PLAYER_CREATE_COMPLETE   = '[create player] COMPLETE'

export const playerCreateAttemptAction = (player : RegisterPlayerCredentials) => ({
    type: ACTION_PLAYER_CREATE_ATTEMPTING,
    payload: player
})


export const playerCreateSuccessAction = (player : IPlayer) => ({
    type: ACTION_PLAYER_CREATE_SUCCESS,
    payload: player
})

export const playerCreateErrorAction = (message : string) => ({
    type: ACTION_PLAYER_CREATE_ERROR,
    payload: message
})

export const playerCreateCompleteAction = () => ({
    type: ACTION_PLAYER_CREATE_COMPLETE
})


// edit player
export const ACTION_PLAYER_UPDATE_ATTEMPTING = '[update player] ATTEMPT'
export const ACTION_PLAYER_UPDATE_SUCCESS    = '[update player] SUCCESS'
export const ACTION_PLAYER_UPDATE_ERROR      = '[update player] ERROR'
export const ACTION_PLAYER_UPDATE_COMPLETE   = '[update player] COMPLETE'

export const playerUpdateAttemptAction = (id : number, player : IEditPlayer) => ({
    type: ACTION_PLAYER_UPDATE_ATTEMPTING,
    current: id,
    payload: player
})

export const playerUpdateSuccessAction = (player : IEditPlayer) => ({
    type: ACTION_PLAYER_UPDATE_SUCCESS,
    payload: player
})

export const playerUpdateErrorAction = (message : string) => ({
    type: ACTION_PLAYER_UPDATE_ERROR,
    payload: message
})

export const playerUpdateCompleteAction = () => ({
    type: ACTION_PLAYER_UPDATE_COMPLETE
})

// get player by id
export const ACTION_PLAYER_GET_ATTEMPTING = '[get(id) player] ATTEMPT'
export const ACTION_PLAYER_GET_SUCCESS    = '[get(id) player] SUCCESS'
export const ACTION_PLAYER_GET_ERROR      = '[get(id) player] ERROR'
export const ACTION_PLAYER_GET_COMPLETE   = '[get(id) player] COMPLETE'

export const playerGetAttemptAction = (id : number) => ({
    type: ACTION_PLAYER_GET_ATTEMPTING,
    payload: id
})

export const playerGetSuccessAction = (player : IPlayer) => ({
    type: ACTION_PLAYER_GET_SUCCESS,
    payload: player
})

export const playerGetErrorAction = (message : string) => ({
    type: ACTION_PLAYER_GET_ERROR,
    payload: message
})

export const playerGetCompleteAction = () => ({
    type: ACTION_PLAYER_GET_COMPLETE
})

//Get all players in Game
export const ACTION_PLAYERINGAME_GET_ATTEMPTING = '[getAllPlayersInGame player] ATTEMPT'
export const ACTION_PLAYERINGAME_GET_SUCCESS    = '[getAllPlayersInGame player] SUCCESS'
export const ACTION_PLAYERINGAME_GET_ERROR      = '[getAllPlayersInGame player] ERROR'
export const ACTION_PLAYERINGAME_GET_COMPLETE   = '[getAllPlayersInGame player] COMPLETE'

export const playerInGameGetAttemptAction = (id : number) => ({
    type: ACTION_PLAYERINGAME_GET_ATTEMPTING,
    payload: id
})

export const playerInGameGetSuccessAction = (player : IPlayer[]) => ({
    type: ACTION_PLAYERINGAME_GET_SUCCESS,
    payload: player
})

export const playerInGameGetErrorAction = (message : string) => ({
    type: ACTION_PLAYERINGAME_GET_ERROR,
    payload: message
})

export const playerInGameGetCompleteAction = () => ({
    type: ACTION_PLAYERINGAME_GET_COMPLETE
})


// get player by id
export const ACTION_PLAYER_DELETE_ATTEMPTING = '[delete(id) player] ATTEMPT'
export const ACTION_PLAYER_DELETE_SUCCESS    = '[delete(id) player] SUCCESS'
export const ACTION_PLAYER_DELETE_ERROR      = '[delete(id) player] ERROR'
export const ACTION_PLAYER_DELETE_COMPLETE   = '[delete(id) player] COMPLETE'

export const playerDeleteAttemptAction = (id : number) => ({
    type: ACTION_PLAYER_DELETE_ATTEMPTING,
    payload: id
})

export const playerDeleteSuccessAction = (player : IPlayer) => ({
    type: ACTION_PLAYER_DELETE_SUCCESS,
    payload: player
})

export const playerDeleteErrorAction = (message : string) => ({
    type: ACTION_PLAYER_DELETE_ERROR,
    payload: message
})

export const playerDeleteCompleteAction = () => ({
    type: ACTION_PLAYER_DELETE_COMPLETE
})