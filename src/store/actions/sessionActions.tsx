import { IPlayer } from "../../types/PlayerTypes"
import { ISquadMember } from "../../types/SquadMemberTypes"
import { ISquad } from "../../types/SquadTypes"
import { IUser } from "../../types/UserTypes"

export const ACTION_SESSION_INIT = '[session] INIT'
export const ACTION_SESSION_SET = '[session] SET'
export const ACTION_SESSION_CLEAR = '[session] CLEAR'
export const ACTION_SESSION_LOGOUT = '[session] LOGOUT'
export const ACTION_SESSION_EXPIRED = '[session] EXPIRED'

export const sessionSetAction = (session: IUser) => ({
    type: ACTION_SESSION_SET,
    payload: session
})

export const sessionExpiredAction = () => ({
    type: ACTION_SESSION_EXPIRED
})

export const sessionLogoutAction = () => ({
    type: ACTION_SESSION_LOGOUT
})

export const sessionInitAction = () => ({
    type: ACTION_SESSION_INIT
})

export const sessionClearAction = () => ({
    type: ACTION_SESSION_CLEAR
})

export const ACTION_PLAYER_SESSION_INIT = '[playerSession] INIT'
export const ACTION_PLAYER_SESSION_SET = '[playerSession] SET'
export const ACTION_PLAYER_SESSION_UPDATE = '[playerSession] UPDATE'
export const ACTION_PLAYER_SESSION_CLEAR = '[playerSession] CLEAR'
export const ACTION_PLAYER_SESSION_BITE = '[playerSession] BITE'
// export const ACTION_ALLPLAYER_SESSION_SET = '[session player] SETALL'
// export const ACTION_ALLPLAYER_SESSION_CLEAR = '[session player] CLEARALL'

export const playerSessionSetAction = (player: IPlayer) => ({
    type: ACTION_PLAYER_SESSION_SET,
    payload: player
})

export const playerSessionInitAction = () => ({
    type: ACTION_PLAYER_SESSION_INIT
})

export const playerSessionClearAction = () => ({
    type: ACTION_PLAYER_SESSION_CLEAR
})

export const playerSessionUpdateAction = (player: IPlayer) => ({
    type: ACTION_PLAYER_SESSION_UPDATE,
    payload: player
})

export const playerSessionBiteAction = (victim: number) => ({
    type: ACTION_PLAYER_SESSION_BITE,
    payload: victim
})

// export const allPlayerSessionSetAction = (session: IPlayer[]) => ({
//     type: ACTION_ALLPLAYER_SESSION_SET,
//     payload: session
// })

// export const allPlayerSessionClearAction = () => ({
//     type: ACTION_ALLPLAYER_SESSION_CLEAR
// })

//Squad session
export const ACTION_SQUADS_SESSION_INIT = '[squadSession] INIT'
export const ACTION_SQUADS_SESSION_SET = '[squadSession] SET'
export const ACTION_SQUADS_SESSION_CLEAR = '[squadSession] CLEAR'
export const ACTION_KILLEDSQUADMEMBER_SESSION_SET = '[killedmemberSession] SET'
export const ACTION_KILLEDSQUADMEMBER_SESSION_CLEAR = '[killedmemberSession] CLEAR'
export const ACTION_SQUADMEMBER_SESSION_SET = '[squadmemberSession] SET'
export const ACTION_SQUADMEMBER_SESSION_CLEAR = '[squadmemberSession] CLEAR'

export const squadsSessionSetAction = (squads: ISquad[]) => ({
    type: ACTION_SQUADS_SESSION_SET,
    payload: squads
})

export const squadsSessionInitAction = () => ({
    type: ACTION_SQUADS_SESSION_INIT
})

export const squadsSessionClearAction = () => ({
    type: ACTION_SQUADS_SESSION_CLEAR
})

export const killedSessionSetAction = (killed: number[]) => ({
    type: ACTION_KILLEDSQUADMEMBER_SESSION_SET,
    payload: killed
})

export const killedSessionClearAction = () => ({
    type: ACTION_KILLEDSQUADMEMBER_SESSION_CLEAR
})

export const squadmemberSessionSetAction = (member: ISquadMember[]) => ({
    type: ACTION_SQUADMEMBER_SESSION_SET,
    payload: member
})

export const squadmemberSessionClearAction = () => ({
    type: ACTION_SQUADMEMBER_SESSION_CLEAR
})