import { IGame, IEditGame } from "../../types/GameTypes"

// fetch all games
export const ACTION_GAMES_FETCH_ATTEMPTING = '[getAll] games ATTEMPT'
export const ACTION_GAMES_FETCH_SUCCESS    = '[getAll] games SUCCESS'
export const ACTION_GAMES_FETCH_ERROR      = '[getAll] games ERROR'
export const ACTION_GAMES_FETCH_COMPLETE   = '[getAll] games COMPLETE'

export const gamesFetchAttemptAction = () => ({
    type: ACTION_GAMES_FETCH_ATTEMPTING
})

export const gamesFetchSuccessAction = (games : IGame[]) => ({
    type: ACTION_GAMES_FETCH_SUCCESS,
    payload: games
})

export const gamesFetchErrorAction = (message : string) => ({
    type: ACTION_GAMES_FETCH_ERROR,
    payload: message
})

export const gamesFetchCompleteAction = () => ({
    type: ACTION_GAMES_FETCH_COMPLETE
})

// fetch one game
export const ACTION_GAME_FETCH_ATTEMPTING = '[get(id)] game ATTEMPT'
export const ACTION_GAME_FETCH_SUCCESS    = '[get(id)] game SUCCESS'
export const ACTION_GAME_FETCH_ERROR      = '[get(id)] game ERROR'
export const ACTION_GAME_FETCH_COMPLETE   = '[get(id)] game COMPLETE'

export const gameFetchAttemptAction = (id : any) => ({
    type: ACTION_GAME_FETCH_ATTEMPTING,
    payload: id
})

export const gameFetchSuccessAction = (game : IGame) => ({
    type: ACTION_GAME_FETCH_SUCCESS,
    payload: game
})

export const gameFetchErrorAction = (message : string) => ({
    type: ACTION_GAME_FETCH_ERROR,
    payload: message
})

export const gameFetchCompleteAction = () => ({
    type: ACTION_GAME_FETCH_COMPLETE
})

// create game
export const ACTION_GAME_CREATE_ATTEMPTING = '[create] game ATTEMPT'
export const ACTION_GAME_CREATE_SUCCESS    = '[create] game SUCCESS'
export const ACTION_GAME_CREATE_ERROR      = '[create] game ERROR'
export const ACTION_GAME_CREATE_COMPLETE   = '[create] game COMPLETE'

export const gameCreateAttemptAction = (game : IEditGame) => ({
    type: ACTION_GAME_CREATE_ATTEMPTING,
    payload: game
})

export const gameCreateSuccessAction = (game : IGame) => ({
    type: ACTION_GAME_CREATE_SUCCESS,
    payload: game
})

export const gameCreateErrorAction = (message : string) => ({
    type: ACTION_GAME_CREATE_ERROR,
    payload: message
})

export const gameCreateCompleteAction = () => ({
    type: ACTION_GAME_CREATE_COMPLETE
})

// edit game
export const ACTION_GAME_UPDATE_ATTEMPTING = '[update] game ATTEMPT'
export const ACTION_GAME_UPDATE_SUCCESS    = '[update] game SUCCESS'
export const ACTION_GAME_UPDATE_ERROR      = '[update] game ERROR'
export const ACTION_GAME_UPDATE_COMPLETE   = '[update] game COMPLETE'

export const gameUpdateAttemptAction = (id : number, game : IEditGame) => ({
    type: ACTION_GAME_UPDATE_ATTEMPTING,
    current: id,
    payload: game
})

export const gameUpdateSuccessAction = () => ({
    type: ACTION_GAME_UPDATE_SUCCESS
})

export const gameUpdateErrorAction = (message : string) => ({
    type: ACTION_GAME_UPDATE_ERROR,
    payload: message
})

export const gameUpdateCompleteAction = () => ({
    type: ACTION_GAME_UPDATE_COMPLETE
})