import { ISquadMember, ICreateSquadMember } from "../../types/SquadMemberTypes"

//Get all SquadMembers
export const ACTION_SQUADMEMBER_GETALL_ATTEMPTING = '[getAll] SquadMember ATTEMPT'
export const ACTION_SQUADMEMBER_GETALL_SUCCESS = '[getAll] SquadMember SUCCESS'
export const ACTION_SQUADMEMBER_GETALL_ERROR = '[getAll] SquadMember ERROR'
export const ACTION_SQUADMEMBER_GETALL_COMPLETE = '[getAll] SquadMember COMPLETE'
export const squadMembersFetchAttemptingAction = () => ({
    type: ACTION_SQUADMEMBER_GETALL_ATTEMPTING
})
export const squadMembersFetchSuccessAction = (squadMembers: ISquadMember[]) => ({
    type: ACTION_SQUADMEMBER_GETALL_SUCCESS,
    payload: squadMembers
})
export const squadMembersFetchErrorAction = (message: string) => ({
    type: ACTION_SQUADMEMBER_GETALL_ERROR,
    payload: message
})
export const squadMembersFetchCompleteAction = () => ({
    type: ACTION_SQUADMEMBER_GETALL_COMPLETE
})

//Get SquadMember by Id
export const ACTION_SQUADMEMBER_GETBYID_ATTEMPTING = '[getById] SquadMember ATTEMPT'
export const ACTION_SQUADMEMBER_GETBYID_SUCCESS = '[getById] SquadMember SUCCESS'
export const ACTION_SQUADMEMBER_GETBYID_ERROR = '[getById] SquadMember ERROR'
export const ACTION_SQUADMEMBER_GETBYID_COMPLETE = '[getById] SquadMember COMPLETE'

export const squadMemberFetchByIdAttemptingAction = (squadMemberId: any) => ({
    type: ACTION_SQUADMEMBER_GETBYID_ATTEMPTING,
    payload: squadMemberId
})
export const squadMemberFetchByIdSuccessAction = (squadMember: ISquadMember) => ({
    type: ACTION_SQUADMEMBER_GETBYID_SUCCESS,
    payload: squadMember
})
export const squadMemberFetchByIdErrorAction = (message: string) => ({
    type: ACTION_SQUADMEMBER_GETBYID_ERROR,
    payload: message
})
export const squadMemberFetchByIdCompleteAction = () => ({
    type: ACTION_SQUADMEMBER_GETBYID_COMPLETE
})

//Create SquadMember
export const ACTION_SQUADMEMBER_CREATE_ATTEMPTING = '[create] SquadMember ATTEMPT'
export const ACTION_SQUADMEMBER_CREATE_SUCCESS = '[create] SquadMember SUCCESS'
export const ACTION_SQUADMEMBER_CREATE_ERROR = '[create] SquadMember ERROR'
export const ACTION_SQUADMEMBER_CREATE_COMPLETE = '[create] SquadMember COMPLETE'

export const squadMemberCreateAttemptingAction = (squadMember: ICreateSquadMember) => ({
    type: ACTION_SQUADMEMBER_CREATE_ATTEMPTING,
    payload: squadMember
})
export const squadMemberCreateSuccessAction = (squadMember: ISquadMember) => ({
    type: ACTION_SQUADMEMBER_CREATE_SUCCESS,
    payload: squadMember
})
export const squadMemberCreateErrorAction = (message: string) => ({
    type: ACTION_SQUADMEMBER_CREATE_ERROR,
    payload: message
})
export const squadMemberCreateCompleteAction = () => ({
    type: ACTION_SQUADMEMBER_CREATE_COMPLETE
})

// get player by id
export const ACTION_SQUADMEMBER_DELETE_ATTEMPTING = '[delete(id) squadmember] ATTEMPT'
export const ACTION_SQUADMEMBER_DELETE_SUCCESS    = '[delete(id) squadmember] SUCCESS'
export const ACTION_SQUADMEMBER_DELETE_ERROR      = '[delete(id) squadmember] ERROR'
export const ACTION_SQUADMEMBER_DELETE_COMPLETE   = '[delete(id) squadmember] COMPLETE'

export const squadMemberDeleteAttemptAction = (id : number) => ({
    type: ACTION_SQUADMEMBER_DELETE_ATTEMPTING,
    payload: id
})

export const squadMemberDeleteSuccessAction = (squadMember: ISquadMember) => ({
    type: ACTION_SQUADMEMBER_DELETE_SUCCESS,
    payload: squadMember
})

export const squadMemberDeleteErrorAction = (message : string) => ({
    type: ACTION_SQUADMEMBER_DELETE_ERROR,
    payload: message
})

export const squadMemberDeleteCompleteAction = () => ({
    type: ACTION_SQUADMEMBER_DELETE_COMPLETE
})