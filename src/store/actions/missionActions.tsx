import { IMission, IEditMission } from "../../types/MissionTypes"



//Get all
export const ACTION_MISSION_GETALL_ATTEMPTING = '[getAll] Mission ATTEMPT'
export const ACTION_MISSION_GETALL_SUCCESS    = '[getAll] Mission SUCCESS'
export const ACTION_MISSION_GETALL_ERROR      = '[getAll] Mission ERROR'
export const ACTION_MISSION_GETALL_COMPLETE   = '[getAll] Mission COMPLETE'


export const missionsFetchAttemptAction = () => ({
    type: ACTION_MISSION_GETALL_ATTEMPTING
})

export const missionsFetchSuccessAction = (missions : IMission[]) => ({
    type: ACTION_MISSION_GETALL_SUCCESS,
    payload: missions
    
})

export const missionsFetchErrorAction = (message : string) => ({
    type: ACTION_MISSION_GETALL_ERROR,
    payload: message
})

export const missionsFetchCompleteAction = () => ({
    type: ACTION_MISSION_GETALL_COMPLETE
})
//Get all by gameId
export const ACTION_MISSION_GETALLBYGAMEID_ATTEMPTING = '[getAllByGameId] Mission ATTEMPT'
export const ACTION_MISSION_GETALLBYGAMEID_SUCCESS    = '[getAllByGameId] Mission SUCCESS'
export const ACTION_MISSION_GETALLBYGAMEID_ERROR      = '[getAllByGameId] Mission ERROR'
export const ACTION_MISSION_GETALLBYGAMEID_COMPLETE   = '[getAllByGameId] Mission COMPLETE'


export const missionsFetchByGameIdAttemptAction = (gameId : any) => ({
    type: ACTION_MISSION_GETALLBYGAMEID_ATTEMPTING,
    payload: gameId
})

export const missionsFetchByGameIdSuccessAction = (missions : IMission[]) => ({
    type: ACTION_MISSION_GETALLBYGAMEID_SUCCESS,
    payload: missions
    
})

export const missionsFetchByGameIdErrorAction = (message : string) => ({
    type: ACTION_MISSION_GETALLBYGAMEID_ERROR,
    payload: message
})

export const missionsFetchByGameIdCompleteAction = () => ({
    type: ACTION_MISSION_GETALLBYGAMEID_COMPLETE
})
// Create
export const ACTION_MISSION_CREATE_ATTEMPTING = '[create] Mission ATTEMPT'
export const ACTION_MISSION_CREATE_SUCCESS    = '[create] Mission SUCCESS'
export const ACTION_MISSION_CREATE_ERROR      = '[create] Mission ERROR'
export const ACTION_MISSION_CREATE_COMPLETE   = '[create] Mission COMPLETE'
export const missionCreateAttemptAction = (mission : IEditMission) => ({
    type: ACTION_MISSION_CREATE_ATTEMPTING,
    payload: mission
})

export const missionCreateSuccessAction = (mission : IMission) => ({
    type: ACTION_MISSION_CREATE_SUCCESS,
    payload: mission
})

export const missionCreateErrorAction = (message : string) => ({
    type: ACTION_MISSION_CREATE_ERROR,
    payload: message
})

export const missionCreateCompleteAction = () => ({
    type: ACTION_MISSION_CREATE_COMPLETE
})

// Delete
export const ACTION_MISSION_DELETE_ATTEMPTING = '[delete] Mission ATTEMPT'
export const ACTION_MISSION_DELETE_SUCCESS    = '[delete] Mission SUCCESS'
export const ACTION_MISSION_DELETE_ERROR      = '[delete] Mission ERROR'
export const ACTION_MISSION_DELETE_COMPLETE   = '[delete] Mission COMPLETE'
export const missionDeleteAttemptAction = (id : any) => ({
    type: ACTION_MISSION_DELETE_ATTEMPTING,
    payload: id
})

export const missionDeleteSuccessAction = (mission : IMission) => ({
    type: ACTION_MISSION_DELETE_SUCCESS,
    payload: mission
})

export const missionDeleteErrorAction = (message : string) => ({
    type: ACTION_MISSION_DELETE_ERROR,
    payload: message
})

export const missionDeleteCompleteAction = () => ({
    type: ACTION_MISSION_DELETE_COMPLETE
})
