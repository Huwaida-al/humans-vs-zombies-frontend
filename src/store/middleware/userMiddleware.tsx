import { UserAPI } from "../../components/api/UserAPI";
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, loginSuccessAction, loginErrorAction,
    ACTION_REGISTER_ATTEMPTING, ACTION_REGISTER_SUCCESS, registerSuccessAction, registerErrorAction } from "../actions/userActions";
import { AnyAction, Middleware, MiddlewareAPI, Dispatch } from "redux";
import { sessionSetAction } from "../actions/sessionActions";
import { IUser } from "../../types/UserTypes";

// login
export const loginMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
        UserAPI.get(action.payload)
        .then(response => {
            if (response) {
                dispatch( loginSuccessAction(response.data) )
            } else {
                dispatch( loginErrorAction("User does not exist") )
            }
        }).catch(error => {
            dispatch(loginErrorAction(error.message))
        })
        
    }
    if (action.type === ACTION_LOGIN_SUCCESS) {
        dispatch(sessionSetAction(action.payload))
    }
}

// register
export const registerUserMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_REGISTER_ATTEMPTING) {
        UserAPI.create(action.payload)
        .then(response => {
            dispatch( registerSuccessAction(response.data) )
        }).catch(error => {
            dispatch(registerErrorAction(error.message))
        })
        
    }
    if (action.type === ACTION_REGISTER_SUCCESS) {
        dispatch(sessionSetAction(action.payload))
    }
}

