import MissionAPI from "../../components/api/MissionAPI";
import { AnyAction, Middleware } from "redux";

import { 
    // Getall
    ACTION_MISSION_GETALL_ATTEMPTING, 
    ACTION_MISSION_GETALL_COMPLETE,
    ACTION_MISSION_GETALL_ERROR, 
    ACTION_MISSION_GETALL_SUCCESS, 

    missionsFetchCompleteAction, 
    missionsFetchErrorAction, 
    missionsFetchSuccessAction,

    // Getall By gameId
    ACTION_MISSION_GETALLBYGAMEID_ATTEMPTING, 
    ACTION_MISSION_GETALLBYGAMEID_COMPLETE,
    ACTION_MISSION_GETALLBYGAMEID_ERROR, 
    ACTION_MISSION_GETALLBYGAMEID_SUCCESS, 

    missionsFetchByGameIdCompleteAction, 
    missionsFetchByGameIdErrorAction, 
    missionsFetchByGameIdSuccessAction,
    // Create
    ACTION_MISSION_CREATE_ATTEMPTING, 
    ACTION_MISSION_CREATE_COMPLETE,
    ACTION_MISSION_CREATE_ERROR, 
    ACTION_MISSION_CREATE_SUCCESS, 

    missionCreateCompleteAction, 
    missionCreateErrorAction, 
    missionCreateSuccessAction, 

    // Delete
    ACTION_MISSION_DELETE_ATTEMPTING, 
    ACTION_MISSION_DELETE_COMPLETE,
    ACTION_MISSION_DELETE_ERROR, 
    ACTION_MISSION_DELETE_SUCCESS, 

    missionDeleteCompleteAction, 
    missionDeleteErrorAction, 
    missionDeleteSuccessAction,
    missionsFetchByGameIdAttemptAction, 
    } from "../actions/missionActions"; 
import { loadStore } from "../../hoc/LocalStorage";

// GetAll not in use
export const getAllMissionsMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_MISSION_GETALL_ATTEMPTING) {
        MissionAPI.getAll()
        .then(response => {
            dispatch(missionsFetchSuccessAction(response.data) )
        }).catch(error => {
            dispatch(missionsFetchErrorAction(error.message))
        })
    }
    if (action.type === ACTION_MISSION_GETALL_SUCCESS) {
    //     let GameId = localStorage.getItem("gameID")
    //     if(GameId){
    //         GameId = JSON.parse(GameId)
    //     }
    //     // localstorage missions + GameId
    //     localStorage.setItem('missions' + GameId?.toString(), JSON.stringify(action.payload));
    }
}
// GetAll by GameId
export const getAllMissionsByGameIdMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)
    
    if (action.type === ACTION_MISSION_GETALLBYGAMEID_ATTEMPTING) {
        MissionAPI.getAllByGameId(action.payload)
        .then(response => {
            dispatch(missionsFetchByGameIdSuccessAction(response.data) )
        }).catch(error => {
            dispatch(missionsFetchByGameIdErrorAction(error.message))
        })
    }
    if (action.type === ACTION_MISSION_GETALLBYGAMEID_SUCCESS) {
        // let GameId = localStorage.getItem("gameID")
        // if(GameId){
        //     GameId = JSON.parse(GameId)
        // }
        // // localstorage missions + GameId
        // localStorage.setItem('missions' + GameId?.toString(), JSON.stringify(action.payload));
        // dispatch(missionsFetchByGameIdCompleteAction());
    }
}
// Create
export const createMissionMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_MISSION_CREATE_ATTEMPTING) {
        MissionAPI.create(action.payload)
        
        .then(response => {
            dispatch( missionCreateSuccessAction(response.data) )
        }).catch(error => {
            dispatch( missionCreateErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_MISSION_CREATE_SUCCESS) {
        let GameId = loadStore("gameID")
        dispatch( missionsFetchByGameIdAttemptAction(GameId));

        dispatch( missionCreateCompleteAction() )
    }
}
// Delete
export const deleteMissionMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_MISSION_DELETE_ATTEMPTING) {
        MissionAPI.remove(action.payload)
        
        .then(response => {
            dispatch( missionDeleteSuccessAction(response.data) )
        }).catch(error => {
            dispatch( missionDeleteErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_MISSION_DELETE_SUCCESS) {
        let GameId = loadStore("gameID")
        dispatch( missionsFetchByGameIdAttemptAction(GameId));

        dispatch( missionDeleteCompleteAction() )
    }
}

