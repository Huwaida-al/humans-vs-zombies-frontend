import SquadMemberAPI from "../../components/api/SquadMemberAPI";
import { AnyAction, Middleware } from "redux";
import { ACTION_SQUADMEMBER_CREATE_ATTEMPTING, ACTION_SQUADMEMBER_CREATE_SUCCESS, ACTION_SQUADMEMBER_DELETE_ATTEMPTING, ACTION_SQUADMEMBER_DELETE_SUCCESS, ACTION_SQUADMEMBER_GETALL_ATTEMPTING, ACTION_SQUADMEMBER_GETBYID_ATTEMPTING, ACTION_SQUADMEMBER_GETBYID_SUCCESS, squadMemberCreateCompleteAction, squadMemberCreateErrorAction, squadMemberCreateSuccessAction, squadMemberDeleteCompleteAction, squadMemberDeleteErrorAction, squadMemberDeleteSuccessAction, squadMemberFetchByIdCompleteAction, squadMemberFetchByIdErrorAction, squadMemberFetchByIdSuccessAction, squadMembersFetchCompleteAction, squadMembersFetchErrorAction, squadMembersFetchSuccessAction } from "../actions/squadMemberActions";
import { ACTION_SQUAD_CREATE_SUCCESS, squadFetchByPlayerIdAttemptingAction, squadFetchByPlayerIdCompleteAction } from "../actions/squadActions";
import { squadmemberSessionSetAction } from "../actions/sessionActions";
import SquadAPI from "../../components/api/SquadAPI";

export const getAllSquadMembersMiddleWare: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_SQUADMEMBER_GETALL_ATTEMPTING) {
        SquadMemberAPI.getAll()
        .then(response => {
            dispatch(squadMembersFetchSuccessAction(response.data))
        }).catch(error => {
            dispatch(squadMembersFetchErrorAction(error.message))
        })
    }
    if (action.type === ACTION_SQUADMEMBER_GETBYID_SUCCESS) {
        localStorage.setItem('squadMembers', JSON.stringify(action.payload));
        dispatch(squadMembersFetchCompleteAction());
    }
}

export const getSquadMemberMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_SQUADMEMBER_GETBYID_ATTEMPTING) {
        SquadMemberAPI.get(action.payload)
        .then(response => {
            dispatch(squadMemberFetchByIdSuccessAction(response.data))
        }).catch(error => {
            dispatch(squadMemberFetchByIdErrorAction(error.message))
        })
    }
    if (action.type === ACTION_SQUADMEMBER_GETBYID_SUCCESS) {
        localStorage.setItem('squadMember', JSON.stringify(action.payload));
        dispatch(squadMemberFetchByIdCompleteAction())
    }
}

export const createSquadMemberMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_SQUADMEMBER_CREATE_ATTEMPTING) {
        SquadMemberAPI.create(action.payload)
        .then(response => {
            console.log(response)
            dispatch( squadMemberCreateSuccessAction(response.data))
        }).catch(error => {
            dispatch(squadMemberCreateErrorAction(error.message))
        })
    }
    if (action.type === ACTION_SQUADMEMBER_CREATE_SUCCESS) {
        dispatch(squadFetchByPlayerIdAttemptingAction(action.payload.playerId));
    }
}

export const deleteSquadMemberMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_SQUADMEMBER_DELETE_ATTEMPTING) {
        SquadMemberAPI.remove(action.payload)
        .then(response => {
            dispatch( squadMemberDeleteSuccessAction(response.data) )
        }).catch(error => {
            dispatch( squadMemberDeleteErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_SQUADMEMBER_DELETE_SUCCESS) {
        dispatch( squadFetchByPlayerIdCompleteAction() );
        dispatch( squadMemberDeleteCompleteAction() );
    }
}