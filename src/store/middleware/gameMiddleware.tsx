import GameAPI from "../../components/api/GameAPI";
import { ACTION_GAMES_FETCH_SUCCESS, ACTION_GAMES_FETCH_ATTEMPTING, gamesFetchSuccessAction, 
    gamesFetchErrorAction, ACTION_GAME_FETCH_ATTEMPTING, gameFetchSuccessAction, 
    gameFetchErrorAction, ACTION_GAME_CREATE_ATTEMPTING, gameCreateSuccessAction, 
    gameCreateErrorAction, ACTION_GAME_CREATE_SUCCESS, gameCreateCompleteAction, 
    ACTION_GAME_FETCH_SUCCESS, 
    gameUpdateCompleteAction,
    gameUpdateErrorAction,
    gameUpdateSuccessAction, ACTION_GAME_UPDATE_ATTEMPTING, gamesFetchCompleteAction, gameFetchCompleteAction, ACTION_GAME_UPDATE_SUCCESS, gamesFetchAttemptAction, gameFetchAttemptAction} from "../actions/gameActions";
import { AnyAction, Middleware } from "redux";

export const getAllGamesMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    console.log(action.type)
    if (action.type === ACTION_GAMES_FETCH_ATTEMPTING) {
        GameAPI.getAll()
        .then(response => {
            dispatch( gamesFetchSuccessAction(response.data) )
        }).catch(error => {
            dispatch(gamesFetchErrorAction(error.message))
        })
    }
    if (action.type === ACTION_GAMES_FETCH_SUCCESS) {
    }
}

export const getGameMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_GAME_FETCH_ATTEMPTING) {
        GameAPI.get(action.payload)
        .then(response => {
            dispatch( gameFetchSuccessAction(response.data) )
        }).catch(error => {
            dispatch( gameFetchErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_GAME_FETCH_SUCCESS) {
    }
}

export const createGameMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if (action.type === ACTION_GAME_CREATE_ATTEMPTING) {
        GameAPI.create(action.payload)
        .then(response => {
            dispatch( gameCreateSuccessAction(response.data) )
        }).catch(error => {
            dispatch( gameCreateErrorAction(error.message) )
        })
    }
    if (action.type === ACTION_GAME_CREATE_SUCCESS) {
        dispatch(gamesFetchAttemptAction());
    }
}

export const updateGameMiddleware: Middleware = ({ dispatch }) => next => (action: AnyAction) => {
    next(action)

    if(action.type === ACTION_GAME_UPDATE_ATTEMPTING){
        GameAPI.update(action.current, action.payload)
        .then(response => {
            dispatch( gameUpdateSuccessAction() )
        }).catch(error => {
            dispatch( gameUpdateErrorAction(error.message) )
        })
    }

    if (action.type === ACTION_GAME_UPDATE_SUCCESS) {
        dispatch(gamesFetchAttemptAction());
    }
        
}

