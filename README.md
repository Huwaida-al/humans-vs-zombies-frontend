# Human Vs Zombies
This project is generated with React Typescript, Redux, Leaflet, Bootstrap and Axios.

## Table of Contents

- [Installation](#installation)
- [Visuals](#visuals)
- [Description](#description)
- [Contributors](#contributors)
- [Project status](#project-status)

## Website-link

   https://morning-meadow-10573.herokuapp.com/

## Installation

      git@gitlab.com:g5452/humans-vs-zombies-frontend.git
      cd humans-vs-zombies-frontend
      npm install 
      npm start

[Navigate to application](http://localhost:3000/) -> http://localhost:3000/

## Visuals

Design of the app in the planing process, drawn in Figma.

![DesignAppPlaning](/img/DesignApp.png)


## Description

Human Vs Zombies is a game of tag played in the real world and it is dedicated to one area. This application gives you the opportunity to socialize and connect to people plaing the same game. When opening the application you will see a list of all games, each game is highly physical and active.

All players except one (or maybe a few more), the patient 0, start as humans, and zombies can then infect human players by “biting” them. Humans win by outliving the zombies, zombies win by tagging all humans.

#### Landing page ("/")

- Game List: A list og all games, each game displays the title, the current game state and number of registered players.
- Register/Login: Auth0 is used to register a user or login a user. When logged in a  ``` Details ``` button will show and redirect the user to GamePage. 

#### Game page ("/game")

- Title: The name, description and rules of the game should be displayed to any user. 
- Registration: A simple button that allows a user to register as a player in this game.
- Map: An interactive map of the game area that contains gravestones and mission markers from time to time. The visibility of individual markers may vary based on the player state.
- Bite Code: When tagged, human players are required to provide a unique, secret bite code to the zombie. The bite codes should be randomly generated and appropriate for manual text entry.
- Bite Code Entry: Zombies that collect the bite code of a human logs their kill in the system to turn the human player into a zombie. 
- Squad Registration: The squad registration fragment displays a small form to create a new squad and a list of available squads to be joined. Each entry in the list displays thier name, respective total number of members, number of “deceased” members and a button to join the squad.
- Squad Details: This displays the names, relative ranks and state of each of the members of your squad. 

#### Chat ("/chat")

- Chat: A simple tabbed, factional chat display with a text field for sending a new message and a button to send. 

#### Admin credetials

When given an administrator account you will have some extra opportunities. 
- Create and edit a game.
- Edit any individual player’s state.
- Create and edit mission markers.


## Contributors

Esben Bjarnason - @EsbenLB

Stian Selle - @kariannet

Karianne Tesaker - @kariannet

Silja Stubhaug Torkildsen - @SiljaTorki

## Project status

The minimal Viable Product for the assignmement is almost complete. 

Features to add/fix:

- Leave squad
- Chat for squads
- Mission check in for squads

## Tech-stack 
Code editor: Visual studio code
React - Quick Start | Redux Toolkit (redux-toolkit.js.org)
Bootstrap // React
Heroku 
